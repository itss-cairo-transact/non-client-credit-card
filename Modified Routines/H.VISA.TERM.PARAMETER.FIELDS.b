*-----------------------------------------------------------------------------
* <Rating>-35</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE H.VISA.TERM.PARAMETER.FIELDS
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : H.VISA.TERM.PARAMETER.FIELDS
*-----------------------------------------------------------------------------------------------
*Description       : Fields for the template H.CARD.ACCOUNT.CATEGORY
*Linked With       : - NA -
*Called From       : - NA -
*In  Parameter     : - NA -
*Out Parameter     : - NA -
*-----------------------------------------------------------------------------------------------
*Modification Details:
*=====================
* Development for Credit Card
*
* PACS00456871        JAHIRHUSSAIN A          26-07-2015                      EPP Easy Payment Plan for Visa card
* *-----------------------------------------------------------------------------------------------
*** <region name= Header>
*** <desc>Inserts and control logic</desc>
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_DataTypes
*** </region>
*-----------------------------------------------------------------------------

*** <region name= DEFINE.FIELDS>
*** <desc> </desc>

    ID.F = 'SYSTEM.ID' ; ID.N = "10" ; ID.T = ''; ID.T<2> = "SYSTEM"
*-----------------------------------------------------------------------------
    neighbour = ""

    fieldName = "FILE.PATH"          ; fieldLength="35" ; fieldType="A" ; GOSUB ADD.FIELDS ;
    fieldName = "XX<DESCRIPTION"     ; fieldLength="35" ; fieldType="A" ; GOSUB ADD.FIELDS ;
    fieldName = "XX-DR.TRANS.CODE"   ; fieldLength="4"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
    CALL Field.setCheckFile("TRANSACTION")

    fieldName = "XX-DR.NARRATIVE"    ; fieldLength="35" ; fieldType="A" ; GOSUB ADD.FIELDS ;
    fieldName = "XX-CR.TRANS.CODE"   ; fieldLength="4"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
    CALL Field.setCheckFile("TRANSACTION")
    fieldName = "XX>CR.NARRATIVE"    ; fieldLength="35" ; fieldType="A" ; GOSUB ADD.FIELDS ;
    fieldName = "INTEREST.ACCT"      ; fieldLength="5"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
    fieldName = "OVER.LINE.ACCT"     ; fieldLength="5"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
    fieldName = "LATE.PAY.ACCT"      ; fieldLength="5"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
    fieldName = "RETENSION.PERIOD"   ; fieldLength="3"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
    CALL Table.addOptionsField("ERR.POST.REST","YES_NO","","")

* PACS00456871 -S
    fieldName = "MISC.DR.ACCT"       ; fieldLength="5"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
* PACS00456871 -E

    fieldName = "BONUS.POINT.ACCT"       ; fieldLength="5"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
    CALL Field.setCheckFile("CATEGORY")
*Non client credit card account	
	fieldName = "POOL.ACCT.NC"      ; fieldLength="16"  ; fieldType="A" ; GOSUB ADD.FIELDS ;
* Audit fields

    GOSUB ADD.AUDIT.FIELDS
    RETURN
*** </region>
*-----------------------------------------------------------------------------
ADD.FIELDS:
    CALL Table.addFieldDefinition(fieldName, fieldLength, fieldType, neighbour)
    RETURN
ADD.RESERVED.FIELDS:
    CALL Table.addField(fieldName, fieldType, args, neighbour)
    RETURN

ADD.AUDIT.FIELDS:
    CALL Table.setAuditPosition
    RETURN
*-----------------------------------------------------------------------------
END
