*-----------------------------------------------------------------------------
* <Rating>-33</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE H.VISA.CLEARING.PARAM.FIELDS
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : H.VISA.CLEARING.PARAM.FIELDS
*-----------------------------------------------------------------------------------------------
*Description       : Fields for the template H.CARD.ACCOUNT.CATEGORY
*Linked With       : - NA -
*Called From       : - NA -
*In  Parameter     : - NA -
*Out Parameter     : - NA -
*-----------------------------------------------------------------------------------------------
*Modification Details:
*=====================
* Development for Credit Card
*
*
* *-----------------------------------------------------------------------------------------------
*** <region name= Header>
*** <desc>Inserts and control logic</desc>
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_DataTypes
*** </region>
*-----------------------------------------------------------------------------

*** <region name= DEFINE.FIELDS>
*** <desc> </desc>

    ID.F = 'SYSTEM.ID' ; ID.N = "10" ; ID.T = ''; ID.T<2> = "SYSTEM"
*-----------------------------------------------------------------------------
    neighbour = ""

    fieldName = "FILE.PATH"                    ;  fieldLength = "35"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    fieldName = "XX.CLAIM.AC.CATEG"               ;  fieldLength = "5"     ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    fieldName = "XX.ARREAR.AC.CATEG"              ;  fieldLength = "5"     ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    fieldName = "XX<VISA.OP.CODE"              ;  fieldLength = "2"     ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
    fieldName = "XX-DESCRIPTION"               ;  fieldLength = "35"    ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
	
    fieldName = "XX-DR.TRANS.CODE"             ;  fieldLength = "4"     ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    CALL Field.setCheckFile("TRANSACTION")
*Add fields for non client credit card
	fieldName = "XX-DR.TRANS.CODE.NC"             ;  fieldLength = "4"     ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    CALL Field.setCheckFile("TRANSACTION")
	
    fieldName = "XX-DR.NARRATIVE"              ;  fieldLength = "35"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    fieldName = "XX-CR.TRANS.CODE"             ;  fieldLength = "4"     ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    CALL Field.setCheckFile("TRANSACTION")
	fieldName = "XX-CR.TRANS.CODE.NC"             ;  fieldLength = "4"     ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    CALL Field.setCheckFile("TRANSACTION")
	
    fieldName = "XX-CR.NARRATIVE"              ;  fieldLength = "35"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    fieldName = "XX-XX<COND.DESC"              ;  fieldLength = "35"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
	
    fieldName = "XX-XX-VISA.REVOLVIN.AC"       ;  fieldLength = "16"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
	fieldName = "XX-XX-VISA.REVOLVIN.AC.NC"       ;  fieldLength = "16"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
	
    fieldName = "XX-XX-MERCH.COMM.AC"          ;  fieldLength = "16"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    fieldName = "XX-XX-MERCH.COMM.AC.NC"          ;  fieldLength = "16"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
	
    fieldName = "XX-XX-FOREX.ACCOUNT"          ;  fieldLength = "16"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
	fieldName = "XX-XX-FOREX.ACCOUNT.NC"          ;  fieldLength = "16"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
	fieldName = "XX-XX-ACCOUNT.NO.NC"          ;  fieldLength = "16"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;
    fieldName = "XX>XX>ACCOUNT.NO"             ;  fieldLength = "16"    ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
	fieldName = "POOL.ACCT.NC"                 ;  fieldLength = "16"    ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
    fieldName = "COMM.PERCENT.1"               ;  fieldLength = "10"    ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
    fieldName = "COMM.PERCENT.2"               ;  fieldLength = "10"    ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
    fieldName = "EXCHANGE.RATE.1"              ;  fieldLength = "10"    ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
    fieldName = "EXCHANGE.RATE.2"              ;  fieldLength = "10"    ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
    fieldName = "RETENTION.PERIOD"             ;  fieldLength = "3"     ;   fieldType = 'A' ;  GOSUB ADD.FIELDS ;
    fieldName = "XX.VALID.IMDS"                ;  fieldLength = "12"    ;   fieldType = "A" ;  GOSUB ADD.FIELDS ;  
    CALL Table.addOptionsField("ERR.POST.REST","YES_NO","","")

* Audit fields

    GOSUB ADD.AUDIT.FIELDS
    RETURN
*** </region>
*-----------------------------------------------------------------------------
ADD.FIELDS:
    CALL Table.addFieldDefinition(fieldName, fieldLength, fieldType, neighbour)
    RETURN
ADD.RESERVED.FIELDS:
    CALL Table.addField(fieldName, fieldType, args, neighbour)
    RETURN

ADD.AUDIT.FIELDS:
    CALL Table.setAuditPosition
    RETURN
*-----------------------------------------------------------------------------
END
