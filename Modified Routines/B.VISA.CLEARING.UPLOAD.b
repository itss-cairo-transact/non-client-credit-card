*-----------------------------------------------------------------------------
* <Rating>-56</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.CLEARING.UPLOAD(Y.LINE)
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.CLEARING.UPLOAD
*-------------------------------------------------------------------------------
*Description       : Pick the records from the clearing file. And based the operation code
*                    corresponding entries to be rised
*Linked With       : BATCH routine
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.CARD.ISSUE
$INSERT I_F.FUNDS.TRANSFER
$INSERT I_F.STMT.ENTRY
$INSERT I_F.H.VISA.CLEARING.FILE
$INSERT I_F.H.VISA.CLEARING.PARAM
$INSERT I_B.VISA.CLEARING.COMMON
$INSERT I_F.H.CREDIT.CARD.LOG
$INSERT I_F.ACCOUNT

    GOSUB PROCESS

    RETURN
*----------------------------------------------------------------------------------
PROCESS:
* Uploading the Values to the Template for reference
*----------------------------------------------------------------------------------
*Nov20th-Dhinesh


    DELIMIT.VALUE = DCOUNT(Y.LINE,'*')
    FNAME = FIELD(Y.LINE,'*',DELIMIT.VALUE)
    Y.LINE = FIELD(Y.LINE,'*',1,DELIMIT.VALUE-1)

    FD.CNT  = DCOUNT(FIELD.NAME,VM)

    FOR INT.VAR = 1 TO FD.CNT
        R.H.VISA.CLEARING.FILE<-1> = Y.LINE[POSITION.ARR<1,INT.VAR>,LENGTH.ARR<1,INT.VAR>]
    NEXT INT.VAR

    CALL ALLOCATE.UNIQUE.TIME(UNIQUE.TIME)
    UNIQUE.TIME  = DATE():UNIQUE.TIME
    TXN.ID       = 'VISA':UNIQUE.TIME

    CALL F.WRITE(FN.H.VISA.CLEARING.FILE,TXN.ID,R.H.VISA.CLEARING.FILE)
    GOSUB DATA.FETCH

    RETURN
*----------------------------------------------------------------------------------
DATA.FETCH:
* Pick the Values from the incoming file
*----------------------------------------------------------------------------------

    LOCATE "VC.OPERATION.CODE" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        Y.OPERATION.CODE = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
        Y.OPERATION.CODE = TRIM(Y.OPERATION.CODE," ","D")
        IF Y.OPERATION.CODE EQ '' THEN
            RETURN
        END
    END

    LOCATE "VC.TRANS.CCY.CODE" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        Y.TRANS.CODE     = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
    END

    LOCATE "VC.AMOUNT.DEBITED" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        DEBIT.AMOUNT     = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
        DEBIT.AMOUNT = TRIM(DEBIT.AMOUNT," ","D")
        DEBIT.AMOUNT = TRIM(DEBIT.AMOUNT,"0","L")
    END

    LOCATE "VC.CLEARED.AMOUNT" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        CLEARED.AMOUNT   = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
        CLEARED.AMOUNT   = TRIM(CLEARED.AMOUNT," ","D")
        CLEARED.AMOUNT   = TRIM(CLEARED.AMOUNT,"0","L")
    END

    LOCATE "VC.TRANS.GROSS.AMT" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        GROSS.AMOUNT     = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
        GROSS.AMOUNT     = TRIM(GROSS.AMOUNT," ","D")
        GROSS.AMOUNT     = TRIM(GROSS.AMOUNT,"0","L")
    END

    LOCATE "VC.CARD.ACC.NO" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        CARD.ACCT        = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
        CARD.ACCT        = TRIM(CARD.ACCT," ","D")
    END

    LOCATE "VC.CARD.HOLDER.NO" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        CARD.NO = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
        CARD.NO = TRIM(CARD.NO," ","D")
    END

    LOCATE "VC.CLR.NO.OF.DEC" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        DECI.PLACE = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
    END

    LOCATE "VC.MERCHANT.CATEG" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        MERCH.CATEG = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
    END

    LOCATE "VC.REMITTANCE" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        REMIT.VAL = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
    END

    LOCATE "VC.ACRONYM" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        ACRONYM = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
    END

    LOCATE "VC.CARD.COMM.FEE" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        COMM.AMOUNT     = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
        COMM.AMOUNT     = TRIM(COMM.AMOUNT," ","D")
        COMM.AMOUNT     = TRIM(COMM.AMOUNT,"0","L")
    END
*RNN for non client credit card -itss	
	LOCATE "VC.MICROFILM.REF" IN R.H.UTILITY.FORMAT.PRINT<1,1> SETTING POS THEN
        RNN = Y.LINE[POSITION.ARR<1,POS>,LENGTH.ARR<1,POS>]
    END
	
    IF CLEARED.AMOUNT THEN
        CLEARED.AMOUNT = CLEARED.AMOUNT DECI.PLACE
    END ELSE
        CLEARED.AMOUNT = 0
    END

    CARD.NO = TRIM(CARD.NO," ","D")
*    CARD.NO = 'VISA.':CARD.NO
*    CALL F.READ(FN.CARD.ISSUE,CARD.NO,R.CARD.ISSUE,F.CARD.ISSUE,F.ERROR)
*    AC.COUNT = DCOUNT(R.CARD.ISSUE<CARD.IS.ACCOUNT>,VM)
*    IF R.CARD.ISSUE EQ '' THEN
*        GOSUB CARD.LOG
*        RETURN
*    END
*    CNT = 0
*    LOOP
*        CNT+=1
*    WHILE CNT LE AC.COUNT
*        ACCT.NO = R.CARD.ISSUE<CARD.IS.ACCOUNT,CNT>
*        CALL F.READ(FN.ACCOUNT,ACCT.NO,R.ACCOUNT,F.ACCOUNT,F.ERROR)
*        THIS.CATEG = R.ACCOUNT<AC.CATEGORY>
*        THIS.CCY = R.ACCOUNT<AC.CURRENCY>
*        BEGIN CASE
*        CASE ARREAR.CATEG EQ THIS.CATEG
*            ARREAR.ACCOUNT = ACCT.NO
*            ARREAR.AC.CCY  = THIS.CCY
*        CASE CLAIM.CATEG EQ THIS.CATEG
*            CLAIM.ACCOUNT = ACCT.NO
*            CLAIM.AC.CCY  = THIS.CCY
*        CASE CLAIM.CATEG NE THIS.CATEG AND ARREAR.CATEG NE THIS.CATEG
*            CUST.ACCOUNT = ACCT.NO
*            CUST.AC.CCY  = THIS.CCY


Y.ACCT.FOUND = ''
* add condition if its non client credit card account - itss
IF CARD.ACCT THEN
*        END CASE
*    REPEAT

TYP.ACCT = CARD.ACCT[1,1]

IF TYP.ACCT EQ '2' THEN


CALL F.READ(FN.ACCOUNT,CARD.ACCT,R.ACCOUNT.NC,F.ACCOUNT,F.ERROR.NC)
		
	
	THIS.CCY   = R.ACCOUNT.NC<AC.CURRENCY>
	
	
	NON.ACCT.NC     = R.H.VISA.CLEARING.PARAM<CL.POOL.ACCT.NC>
		
	CLAIM.ACCOUNT = THIS.CCY:NON.ACCT.NC
    CLAIM.AC.CCY  = THIS.CCY
	

   
    CUST.ACCOUNT = CARD.ACCT
    CUST.AC.CCY  = THIS.CCY
	
	CALL F.READ(FN.ACCOUNT,CLAIM.ACCOUNT,R.ACCOUNT.POLL,F.ACCOUNT,F.ERROR.POOL)
	IF R.ACCOUNT.POLL NE '' THEN
	THIS.CATEG = R.ACCOUNT.POLL<AC.CATEGORY>
	Y.ACCT.FOUND = 1
	END

END ELSE
    R.CARD.ISSUE.ACCOUNT = '' ; Y.CARD.ISSUE.ACCOUNT.ERR = ''
    CALL F.READ(FN.CARD.ISSUE.ACCOUNT,CARD.ACCT,R.CARD.ISSUE.ACCOUNT,F.CARD.ISSUE.ACCOUNT,Y.CARD.ISSUE.ACCOUNT.ERR)
	
	LOOP
		REMOVE Y.CARD.CURR.NO FROM R.CARD.ISSUE.ACCOUNT SETTING Y.CARD.ISS.POS
	WHILE Y.CARD.CURR.NO:Y.CARD.ISS.POS
	    IF Y.CARD.CURR.NO[1,4] EQ 'VISA' THEN
		    IF Y.CARD.CURR.NO[18,4] EQ CARD.NO[13,4] THEN
			    CARD.NO = Y.CARD.CURR.NO
				R.CARD.ISSUE = '' ; F.ERROR = ''
				CALL F.READ(FN.CARD.ISSUE,CARD.NO,R.CARD.ISSUE,F.CARD.ISSUE,F.ERROR)
				AC.COUNT = DCOUNT(R.CARD.ISSUE<CARD.IS.ACCOUNT>,VM)
				IF R.CARD.ISSUE EQ '' THEN
				    GOSUB CARD.LOG
				    CONTINUE
				END
				LOOP
				    CNT+=1
				WHILE CNT LE AC.COUNT
				    ACCT.NO = R.CARD.ISSUE<CARD.IS.ACCOUNT,CNT>
					CALL F.READ(FN.ACCOUNT,ACCT.NO,R.ACCOUNT,F.ACCOUNT,F.ERROR)
					THIS.CATEG = R.ACCOUNT<AC.CATEGORY>
					THIS.CCY = R.ACCOUNT<AC.CURRENCY>
					BEGIN CASE
					CASE THIS.CATEG MATCHES ARREAR.CATEG
					    ARREAR.ACCOUNT = ACCT.NO
						ARREAR.AC.CCY  = THIS.CCY
						Y.ACCT.FOUND = 1
					CASE THIS.CATEG MATCHES CLAIM.CATEG
                        CLAIM.ACCOUNT = ACCT.NO
                        CLAIM.AC.CCY  = THIS.CCY
						Y.ACCT.FOUND = 1
                    CASE NOT(THIS.CATEG MATCHES CLAIM.CATEG) AND NOT(THIS.CATEG MATCHES ARREAR.CATEG)
                        CUST.ACCOUNT = ACCT.NO
                        CUST.AC.CCY  = THIS.CCY
						Y.ACCT.FOUND = 1
					END CASE	
				REPEAT
			END ELSE
			    CONTINUE
			END
		END
	REPEAT
END ELSE
    RETURN
END

END

*add condition if account fount -itss    
        *IF REMIT.VAL NE 'R' THEN
	    IF REMIT.VAL NE 'R' AND Y.ACCT.FOUND THEN
        GOSUB CONDITION.CHECK
    END

    GOSUB CLEAR.VARIABLES

    RETURN
*----------------------------------------------------------------------------------
CLEAR.VARIABLES:
* Clearing variables for each record
*----------------------------------------------------------------------------------
    ARREAR.ACCOUNT = '' ; CLAIM.ACCOUNT = '' ; CUST.ACCOUNT ='' ; ARREAR.AC.CCY = '' ; CLAIM.AC.CCY='' ; CUST.AC.CCY ='' ;
    CARD.NO = '' ; AC.COUNT = '' ;  REMIT.VAL = ''; Y.TRANS.CODE = '' ; DEBIT.AMOUNT = '' ; CLEARED.AMOUNT = '' ;
    GROSS.AMOUNT='' ; CARD.ACCT ='' ;CARD.NO='' ; DECI.PLACE = '' ; MERCH.CATEG = ''; REMIT.VAL = '' ; Y.OPERATION.CODE = '' ;

    RETURN
*----------------------------------------------------------------------------------
CARD.LOG:
* Updating the failure log
*----------------------------------------------------------------------------------
    R.H.CREDIT.CARD.LOG<CARD.LOG.CARD.TYPE>        = 'VISA'
    R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.NAME>        = FNAME
    R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.TYPE>        = 'Clearing_File'
    R.H.CREDIT.CARD.LOG<CARD.LOG.TRANSACTION.REF>  = TXN.ID

    R.H.CREDIT.CARD.LOG<CARD.LOG.TRANS.STATUS>     = 'FAILURE'
    R.H.CREDIT.CARD.LOG<CARD.LOG.ERR.MSG>          = "INVALID CARD NUMBER"
    R.H.CREDIT.CARD.LOG<CARD.LOG.DATE>             = TODAY

    CALL ALLOCATE.UNIQUE.TIME(UNIQUE.TIME)
    UNIQUE.TIME = DATE():UNIQUE.TIME
    CREDIT.LOG.ID = UNIQUE.TIME

    CALL F.WRITE(FN.H.CREDIT.CARD.LOG,CREDIT.LOG.ID,R.H.CREDIT.CARD.LOG)

    RETURN

*----------------------------------------------------------------------------------
CONDITION.CHECK:
* Check for the operation code and call the corresponding operation code routines
*----------------------------------------------------------------------------------

    SAVE.CLAIM.CATEG = CLAIM.CATEG
    CLAIM.CATEG = THIS.CATEG

    BEGIN CASE
    CASE Y.OPERATION.CODE EQ '01'
        CALL B.VISA.OP.CODE.01
    CASE Y.OPERATION.CODE EQ '02'
        CALL B.VISA.OP.CODE.02
    CASE Y.OPERATION.CODE EQ '03'
        CALL B.VISA.OP.CODE.03
    CASE Y.OPERATION.CODE EQ '05'
        CALL B.VISA.OP.CODE.05
    CASE Y.OPERATION.CODE EQ '06'
        CALL B.VISA.OP.CODE.06
    CASE Y.OPERATION.CODE EQ '07'
        CALL B.VISA.OP.CODE.07
    CASE Y.OPERATION.CODE EQ '13'
        CALL B.VISA.OP.CODE.13
    CASE Y.OPERATION.CODE EQ '25'
        CALL B.VISA.OP.CODE.25
    CASE Y.OPERATION.CODE EQ '26'
        CALL B.VISA.OP.CODE.26
    CASE Y.OPERATION.CODE EQ '27'
        CALL B.VISA.OP.CODE.27
    CASE Y.OPERATION.CODE EQ ''
        RETURN
    END CASE

    CLAIM.CATEG = SAVE.CLAIM.CATEG

    RETURN
*----------------------------------------------------------------------------------
