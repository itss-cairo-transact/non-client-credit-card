*-----------------------------------------------------------------------------
* <Rating>-91</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.OP.CODE.05
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.OP.CODE.05
*-------------------------------------------------------------------------------
*Description       : Routine which raise the accounting entries for op code 05
*Linked With       :
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.ACCOUNT
$INSERT I_F.STMT.ENTRY
$INSERT I_F.CURRENCY
$INSERT I_F.FUNDS.TRANSFER
$INSERT I_F.USER
$INSERT I_F.H.VISA.CLEARING.FILE
$INSERT I_F.H.VISA.CLEARING.PARAM
$INSERT I_F.H.CREDIT.CARD.LOG
$INSERT I_B.VISA.CLEARING.COMMON


    GOSUB INIT
	IF ACRONYM[1,22] NE 'eFAWATERcom bill payme' THEN
		GOSUB PROCESS
	END

    RETURN
*----------------------------------------------------------------------------------
INIT:
*----------------------------------------------------------------------------------

    LOCATE "05" IN R.H.VISA.CLEARING.PARAM<CL.VISA.OP.CODE,1> SETTING POS THEN
        DR.TRANS.CODE   = R.H.VISA.CLEARING.PARAM<CL.DR.TRANS.CODE,POS>
        CR.TRANS.CODE   = R.H.VISA.CLEARING.PARAM<CL.CR.TRANS.CODE,POS>
        DR.NARRATIVE    = ACRONYM
        CR.NARRATIVE    = ACRONYM

*        DR.NARRATIVE    = R.H.VISA.CLEARING.PARAM<CL.DR.NARRATIVE,POS>
*        CR.NARRATIVE    = R.H.VISA.CLEARING.PARAM<CL.CR.NARRATIVE,POS>

        COND.DESC       = R.H.VISA.CLEARING.PARAM<CL.COND.DESC,POS>
        VISA.REVL.ACCT  = R.H.VISA.CLEARING.PARAM<CL.VISA.REVOLVIN.AC,POS>
        FOREX.ACCT      = R.H.VISA.CLEARING.PARAM<CL.FOREX.ACCOUNT,POS>
    END

    V       = FT.AUDIT.DATE.TIME
    FT.POS  = V
    FN.CURR = 'F.CURRENCY'
    F.CURR  = ''
    CALL OPF(FN.CURR,F.CURR)

    CHANGE SM TO VM IN VISA.REVL.ACCT
    CHANGE SM TO VM IN FOREX.ACCT

    CALL F.READ(FN.ACCOUNT,CUST.ACCOUNT,R.ACCOUNT.1,F.ACCOUNT,F.ERR)
    CUST.AC.CCY = R.ACCOUNT.1<AC.CURRENCY>
    CUST.CATEG  = R.ACCOUNT.1<AC.CATEGORY>

    RETURN
*----------------------------------------------------------------------------------
ACCT.FETCH.1:
*----------------------------------------------------------------------------------
    VISA.REVL.AC   = VISA.REVL.ACCT<1,1>
    CALL F.READ(FN.ACCOUNT,VISA.REVL.AC,R.ACCOUNT.2,F.ACCOUNT,F.ERR)
    VISA.REVL.CCY   = R.ACCOUNT.2<AC.CURRENCY>
    VISA.REVL.CATEG = R.ACCOUNT.2<AC.CATEGORY>

    FOREX.AC = FOREX.ACCT<1,1>
    CALL F.READ(FN.ACCOUNT,FOREX.AC,R.ACCOUNT.3,F.ACCOUNT,F.ERR)
    FOREX.AC.CCY   = R.ACCOUNT.3<AC.CURRENCY>
    FOREX.AC.CATEG = R.ACCOUNT.3<AC.CATEGORY>

    RETURN
*----------------------------------------------------------------------------------
PROCESS:
*----------------------------------------------------------------------------------

    DR.ACC    =''
    DR.CCY    =''
    DR.CATEG  =''
    CR.ACC    =''
    CR.CCY    =''
    CR.CATEG  =''
    PL.CATEG  =''
*    SEL.CCY ='SELECT ':FN.CURR:' WITH NUMERIC.CCY.CODE EQ ':Y.TRANS.CODE
*    CALL EB.READLIST(SEL.CCY,SEL.LIST,'',NO.REC,SEL.ERR)
*    CALL F.READ(FN.CURR,SEL.LIST,R.CURR,F.CURR,F.ERR)
    CALL F.READ(FN.NUMERIC.CURRENCY,Y.TRANS.CODE,R.NUMERIC.CURRENCY,F.NUMERIC.CURRENCY,ERR.NUM.CCY)
    CALL F.READ(FN.CURR,R.NUMERIC.CURRENCY,R.CURR,F.CURR,F.ERR)

    DE.PLACE = R.CURR<EB.CUR.NO.OF.DECIMALS>
    GROSS.AMOUNT = GROSS.AMOUNT / PWR(10,DE.PLACE)
    DEBIT.AMOUNT = DEBIT.AMOUNT / PWR(10,3)
    CLEARED.AMOUNT = CLEARED.AMOUNT / PWR(10,DECI.PLACE)

    GOSUB ACCT.FETCH.1
    DR.ACC   = CLAIM.ACCOUNT
    DR.CCY   = CLAIM.AC.CCY
    DR.CATEG = CLAIM.CATEG
*    AMOUNT = DEBIT.AMOUNT
	AMOUNT = CLEARED.AMOUNT
    DR.ACCT = DR.ACC
    GOSUB DEBIT.LEG
*    CR.ACC   = VISA.REVL.AC
*    CR.CCY   = VISA.REVL.CCY
	IF CLAIM.AC.CCY EQ 'JOD' THEN
		CR.ACC	 = VISA.REVL.AC
	END ELSE
		CR.ACC	 = CLAIM.AC.CCY:VISA.REVL.AC[4,99]
	END
	CR.CCY   = DR.CCY	
    CR.CATEG = VISA.REVL.CATEG
    AMOUNT   = CLEARED.AMOUNT
    CR.ACCT = CR.ACC
    GOSUB CREDIT.LEG
    CR.ACC   = FOREX.AC
    CR.CCY   = FOREX.AC.CCY
    CR.CATEG = FOREX.AC.CATEG
    CR.ACCT  = CR.ACC
    IF CR.ACC[1,2] EQ 'PL' THEN
        PL.CATEG = CR.ACC[3,7]
        CR.CATEG = PL.CATEG
        CR.ACC   = ''
        CR.CCY   = LCCY
        CR.ACCT = 'PL':PL.CATEG
    END

* To Avoid .001 Diff. Subtract the DEBIT.AMOUNT and CLEARED.AMOUNT in JOD and Credit Foreign exchange account
*    AMOUNT   = (CLEARED.AMOUNT * EXCH.RATE.2) - (CLEARED.AMOUNT * EXCH.RATE.1)

*    IF VISA.REVL.CCY NE LCCY THEN
*        CLEAR.AMT.LCY = CLEARED.AMOUNT * EXCH.RATE.1
*        CALL EB.ROUND.AMOUNT(LCCY,CLEAR.AMT.LCY,'','')
*        AMOUNT = DEBIT.AMOUNT - CLEAR.AMT.LCY
*    END ELSE
*        AMOUNT = DEBIT.AMOUNT - CLEARED.AMOUNT
*    END

*    GOSUB CREDIT.LEG
    GOSUB RAISE.ENTRY

    RETURN
*----------------------------------------------------------------------------------
DEBIT.LEG:
*----------------------------------------------------------------------------------
    DR.ARRAY = ''
    DR.ARRAY<AC.STE.COMPANY.CODE>       = ID.COMPANY
    DR.ARRAY<AC.STE.CURRENCY>           = DR.CCY
    DR.ARRAY<AC.STE.TRANSACTION.CODE>   = DR.TRANS.CODE
    DR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = DR.ACC
    DR.ARRAY<AC.STE.ACCOUNT.OFFICER>    = R.USER<EB.USE.DEPARTMENT.CODE>
    DR.ARRAY<AC.STE.PRODUCT.CATEGORY>   = DR.CATEG
    DR.ARRAY<AC.STE.VALUE.DATE>         = TODAY
    DR.ARRAY<AC.STE.POSITION.TYPE>      = 'TR'
    DR.ARRAY<AC.STE.DEPARTMENT.CODE>    = R.USER<EB.USE.DEPARTMENT.CODE>
    DR.ARRAY<AC.STE.CURRENCY.MARKET>    = '1'
    DR.ARRAY<AC.STE.PL.CATEGORY>        = PL.CATEG
*    DR.ARRAY<AC.STE.OUR.REFERENCE>      = DR.NARRATIVE
    DR.ARRAY<AC.STE.TRANS.REFERENCE>    = ''
    DR.ARRAY<AC.STE.SYSTEM.ID>          = 'AC'
    DR.ARRAY<AC.STE.BOOKING.DATE>       = TODAY
    DR.ARRAY<AC.STE.EXPOSURE.DATE>      = TODAY
    DR.ARRAY<AC.STE.NARRATIVE>          = DR.NARRATIVE
    CALL EB.ROUND.AMOUNT(DR.CCY,AMOUNT,'','')
    AMOUNT                              = ABS(AMOUNT)
    IF DR.CCY EQ LCCY THEN
        DR.ARRAY<AC.STE.AMOUNT.LCY>     = (-1) * AMOUNT
        R.H.CREDIT.CARD.LOG<CARD.LOG.DR.AMOUNT,-1> = AMOUNT
    END ELSE
        Y.CCY = DR.CCY
        GOSUB CONV.CCY
*        YR.AMT.LCY = AMOUNT * EXCH.RATE.1
        CALL EB.ROUND.AMOUNT(LCCY,YR.AMT.LCY,'','')
*        YR.AMT.FCY = AMOUNT
        CALL EB.ROUND.AMOUNT(DR.CCY,YR.AMT.FCY,'','')
        DR.ARRAY<AC.STE.AMOUNT.LCY>     = (-1) * YR.AMT.LCY
        DR.ARRAY<AC.STE.AMOUNT.FCY>     = (-1) * YR.AMT.FCY
        R.H.CREDIT.CARD.LOG<CARD.LOG.DR.AMOUNT,-1> = CLEARED.AMOUNT
    END
    DR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = DR.ACC
    R.H.CREDIT.CARD.LOG<CARD.LOG.VISA.DR.ACC,-1> = DR.ACCT

    MULTI.STMT<-1> = LOWER(DR.ARRAY)

    RETURN
*----------------------------------------------------------------------------------
CREDIT.LEG:
*----------------------------------------------------------------------------------
    CR.ARRAY = ''
    CR.ARRAY<AC.STE.COMPANY.CODE>       = ID.COMPANY
    CR.ARRAY<AC.STE.CURRENCY>           = CR.CCY
    CR.ARRAY<AC.STE.TRANSACTION.CODE>   = CR.TRANS.CODE
    CR.ARRAY<AC.STE.ACCOUNT.OFFICER>    = R.USER<EB.USE.DEPARTMENT.CODE>
    CR.ARRAY<AC.STE.PRODUCT.CATEGORY>   = CR.CATEG
    CR.ARRAY<AC.STE.VALUE.DATE>         = TODAY
    CR.ARRAY<AC.STE.POSITION.TYPE>      = 'TR'
    CR.ARRAY<AC.STE.DEPARTMENT.CODE>    = R.USER<EB.USE.DEPARTMENT.CODE>
    CR.ARRAY<AC.STE.CURRENCY.MARKET>    = '1'
    CR.ARRAY<AC.STE.PL.CATEGORY>        = PL.CATEG
    CR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = CR.ACC
*    CR.ARRAY<AC.STE.OUR.REFERENCE>      = CR.NARRATIVE
    CR.ARRAY<AC.STE.TRANS.REFERENCE>    = ''
    CR.ARRAY<AC.STE.SYSTEM.ID>          = 'AC'
    CR.ARRAY<AC.STE.BOOKING.DATE>       = TODAY
    CR.ARRAY<AC.STE.EXPOSURE.DATE>      = TODAY
    CR.ARRAY<AC.STE.NARRATIVE>          = CR.NARRATIVE
    CALL EB.ROUND.AMOUNT(LCCY,AMOUNT,'','')
    AMOUNT                              = ABS(AMOUNT)
    IF CR.CCY EQ LCCY THEN
        CR.ARRAY<AC.STE.AMOUNT.LCY>     = AMOUNT
        R.H.CREDIT.CARD.LOG<CARD.LOG.CR.AMOUNT,-1> = AMOUNT
    END ELSE
        Y.CCY = CR.CCY
        GOSUB CONV.CCY
*        YR.AMT.LCY = AMOUNT * EXCH.RATE.1
        CALL EB.ROUND.AMOUNT(LCCY,YR.AMT.LCY,'','')
*        YR.AMT.FCY = AMOUNT
        CALL EB.ROUND.AMOUNT(CR.CCY,YR.AMT.FCY,'','')
        CR.ARRAY<AC.STE.AMOUNT.LCY>     = YR.AMT.LCY
        CR.ARRAY<AC.STE.AMOUNT.FCY>     = YR.AMT.FCY
        R.H.CREDIT.CARD.LOG<CARD.LOG.CR.AMOUNT,-1> = YR.AMT.FCY
    END
    R.H.CREDIT.CARD.LOG<CARD.LOG.VISA.CR.ACC,-1> = CR.ACCT
    MULTI.STMT<-1> = LOWER(CR.ARRAY)

    RETURN
*-----------------------------------------------------------------------------
CONV.CCY:
*-----------------------------------------------------------------------------
    BUY.CCY   = Y.CCY
    SELL.CCY  = LCCY
    CCY.MKT   = 1
    BUY.AMT   = ABS(CLEARED.AMOUNT)
    SELL.AMT  = '' ; BASE.CCY = ''
    DIFF = '' ; LCY.AMT = '' ; ERR = '' ; RATE = ''
    CALL EXCHRATE(CCY.MKT,BUY.CCY,BUY.AMT,SELL.CCY,SELL.AMT,BASE.CCY,RATE,DIFF,LCY.AMT,ERR)
    YR.AMT.LCY = LCY.AMT
    YR.AMT.FCY = CLEARED.AMOUNT
    YR.RATE    = RATE

    RETURN

*----------------------------------------------------------------------------------
RAISE.ENTRY:
*----------------------------------------------------------------------------------

    ID.NEW = TXN.ID
    SYSTEM.ID = 'AC'
    TYPE = 'SAO'
    ENTRY.ARRAY = MULTI.STMT
    ACCOUNTING.TYPE  = ""
    ERR.REC = ""
    CALL EB.ACCOUNTING.CHECK(SYSTEM.ID,TYPE,ENTRY.ARRAY,ACCOUNTING.TYPE,ERR.REC)

    IF ERR.REC EQ '' THEN
        CALL EB.ACCOUNTING.WRAPPER(SYSTEM.ID,TYPE,ENTRY.ARRAY,ACCOUNTING.TYPE,ERR.REC)
    END
    GOSUB CARD.LOG

    RETURN
*-----------------------------------------------------------------------------
CARD.LOG:
*-----------------------------------------------------------------------------

    R.H.CREDIT.CARD.LOG<CARD.LOG.CARD.TYPE>        = 'VISA'
    R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.NAME>        = FNAME
    R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.TYPE>        = 'Clearing_File'
    R.H.CREDIT.CARD.LOG<CARD.LOG.TRANSACTION.REF>  = TXN.ID
    IF ERR.REC NE '' THEN
        R.H.CREDIT.CARD.LOG<CARD.LOG.TRANS.STATUS> = 'FAILURE'
        R.H.CREDIT.CARD.LOG<CARD.LOG.ERR.MSG>      = ERR.REC
    END ELSE
        STMT.POS = FT.POS - 10
        R.H.CREDIT.CARD.LOG<CARD.LOG.STMT.ID>      = R.NEW(STMT.POS)
        R.H.CREDIT.CARD.LOG<CARD.LOG.TRANS.STATUS> = 'SUCCESS'
    END
    R.H.CREDIT.CARD.LOG<CARD.LOG.DATE>             = TODAY
    R.H.CREDIT.CARD.LOG<CARD.LOG.NARRATIVE>        = ACRONYM

    CALL ALLOCATE.UNIQUE.TIME(UNIQUE.TIME)
    UNIQUE.TIME = DATE():UNIQUE.TIME
    CREDIT.LOG.ID = UNIQUE.TIME

    CALL F.WRITE(FN.H.CREDIT.CARD.LOG,CREDIT.LOG.ID,R.H.CREDIT.CARD.LOG)

    RETURN
*-----------------------------------------------------------------------------
END
