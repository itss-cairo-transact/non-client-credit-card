*-----------------------------------------------------------------------------
* <Rating>-56</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.CLEARING.PRELOG.SELECT
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.CLEARING.PRELOG.SELECT
*-------------------------------------------------------------------------------
*Description       : Select routine to select the records in the file placed in the path
*Linked With       : BNK/B.VISA.CLEARING.PRELOG - batch
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.H.VISA.CLEARING.FILE
$INSERT I_B.VISA.CLEARING.COMMON

    GOSUB PROCESS

    RETURN
*----------------------------------------------------------------------------------
PROCESS:
* Select the file in the file path. and select the unprocessed text file
* rename it to filename.processed
*----------------------------------------------------------------------------------


    FN.FILE.OUT.PATH  = FILE.PATH
    F.FILE.OUT.PATH   = ''
    CALL OPF(FN.FILE.OUT.PATH,F.FILE.OUT.PATH)

    LINEFEED = CHAR(10)
*    SEL.STMT='SELECT ':FN.FILE.OUT.PATH:' WITH @ID UNLIKE ...Processed AND @ID UNLIKE ...Validated'
    SEL.STMT='SELECT ':FN.FILE.OUT.PATH
    CALL EB.READLIST(SEL.STMT,SEL.LIST,'',NO.OF.REC,SEL.ERR)

    LOOP

	REMOVE CURR.SEL.ID FROM SEL.LIST SETTING CURR.POS
    WHILE CURR.SEL.ID NE ''

IF CURR.SEL.ID[9] NE 'Processed' AND CURR.SEL.ID[9] NE 'Validated' THEN

    FNAME = CURR.SEL.ID
    CALL F.READ(FN.FILE.OUT.PATH,FNAME,R.DATA.LIST,F.FILE.OUT.PATH,F.ERROR)

    FNAME.DUP = FIELD(FNAME,'.',1)      ;*PACS00394374
    CHANGE LINEFEED TO FM IN R.DATA.LIST
* PACS00394374 -S
    SEL.STMT.DUP='SELECT ':FN.H.CREDIT.CARD.LOG:' WITH FILE.NAME EQ ':FNAME.DUP:
    CALL EB.READLIST(SEL.STMT.DUP,SEL.LIST.DUP,'',NOS,SEL.ERR)
    IF SEL.LIST.DUP THEN
        FLAG.DUP = "YES"
        CALL OCOMO("This file has been processed already")
        RETURN

    END
* PACS00394374 -E

    SEL.STMT='SELECT ':FN.H.CREDIT.CARD.LOG:' WITH FILE.NAME EQ ':FNAME:' AND MARKER EQ ':"Validated"
    CALL EB.READLIST(SEL.STMT,SEL.LIST.1,'',NOS,SEL.ERR)

    LOOP
        REMOVE Y.ID FROM SEL.LIST.1 SETTING POS
    WHILE Y.ID:POS
        CALL F.DELETE(FN.H.CREDIT.CARD.LOG,Y.ID)
    REPEAT

    LOOP
        REMOVE LINES FROM R.DATA.LIST SETTING LINES.POS
    WHILE LINES:LINES.POS
        OUR.LIST<-1> = LINES:'*':FNAME
    REPEAT

    CALL BATCH.BUILD.LIST('',OUR.LIST)

    FILE.NAME = FNAME:'.Validated'
    IF R.DATA.LIST THEN
        CALL F.WRITE(FN.FILE.OUT.PATH,FILE.NAME,R.DATA.LIST)
    END
    CALL F.DELETE(FN.FILE.OUT.PATH,SEL.LIST<1,1>)

END

REPEAT

    RETURN
