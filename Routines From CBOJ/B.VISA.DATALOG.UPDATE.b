*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.DATALOG.UPDATE
*-----------------------------------------------------------------------------
*Company Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program Name    : B.VISA.DATALOG.UPDATE
*-------------------------------------------------------------------------------
*Description       : Updating the Error and Override message Log
*Linked With       : BNK/B.VISA.DATA.PRELOG
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2011-02-0055
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.USER
$INSERT I_F.H.CREDIT.CARD.LOG
$INSERT I_B.VISA.DATA.COMMON
$INSERT I_F.PRELOG.CREDIT.CARD

    GOSUB INIT
    GOSUB PROCESS

    RETURN
*---------------------------------------------------------------------------------
INIT:
*---------------------------------------------------------------------------------
DEBUG   
 FN.H.CREDIT.CARD.LOG='F.H.CREDIT.CARD.LOG'
    F.H.CREDIT.CARD.LOG=''
    CALL OPF(FN.H.CREDIT.CARD.LOG,F.H.CREDIT.CARD.LOG)

    FN.PRELOG.CREDIT.CARD.NAU='F.PRELOG.CREDIT.CARD$NAU'
    F.PRELOG.CREDIT.CARD.NAU=''
    CALL OPF(FN.PRELOG.CREDIT.CARD.NAU,F.PRELOG.CREDIT.CARD.NAU)

    DT = OCONV(DATE(),"D4-")
    TM = OCONV(TIME(),"MTS")

    DAY = DT[4,2]
    MONTH = DT[1,2]
    YEAR = DT[9,2]
    HOURS = TM[1,2]
    MIN = TM[4,2]
    DT = YEAR:MONTH:DAY
    TM = HOURS:MIN
    DT.TM = DT:TM
    ERR.MSG.CNT=0
    OVR.MSG.CNT=0

    RETURN
*---------------------------------------------------------------------------------
PROCESS:
*---------------------------------------------------------------------------------
    SEL.STMT='SELECT ':FN.H.CREDIT.CARD.LOG:' WITH FILE.TYPE EQ ':"Term_Data_File":' AND CARD.TYPE EQ ':"VISA":' AND FILE.NAME EQ ':FNAME

    CALL EB.READLIST(SEL.STMT,SEL.LIST,'',SEL.CNT,SEL.ERR)
    LOOP
        REMOVE Y.ID FROM SEL.LIST SETTING POS
    WHILE Y.ID:POS
        CALL F.READ(FN.H.CREDIT.CARD.LOG,Y.ID,R.H.CREDIT.CARD.LOG,F.H.CREDIT.CARD.LOG,F.ERR)
        CAR.TYP.NME =R.H.CREDIT.CARD.LOG<CARD.LOG.CARD.TYPE>
        CAR.FLE.NME =R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.NAME>
        CAR.FLE.TYP =R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.TYPE>
        CAR.POS.DTE =R.H.CREDIT.CARD.LOG<CARD.LOG.DATE>
        ERR.MSG.CNT+=DCOUNT(R.H.CREDIT.CARD.LOG<CARD.LOG.ERR.MSG>,VM)
        OVR.MSG.CNT+=DCOUNT(R.H.CREDIT.CARD.LOG<CARD.LOG.OVR.MSG>,VM)
    REPEAT
    GOSUB WRITE.LOG
    RETURN
*---------------------------------------------------------------------------------
WRITE.LOG:
*---------------------------------------------------------------------------------
    FILE.ID="TD.":FNAME:'-':TODAY
    CALL F.READU(FN.PRELOG.CREDIT.CARD.NAU,FILE.ID,R.PRELOG.CREDIT.CARD.NAU,F.PRELOG.CREDIT.CARD.NAU,F.ERR,RTRY)

    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.CARD.TYPE>   = CAR.TYP.NME
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.FILE.NAME>   = CAR.FLE.NME
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.FILE.TYPE>   = CAR.FLE.TYP
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.DATE>        = CAR.POS.DTE
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.ERROR.COUNT> = ERR.MSG.CNT
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.WARNING.CNT> = OVR.MSG.CNT

    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.RECORD.STATUS>='INAU'
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.CURR.NO> = '1'
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.INPUTTER> = TNO:'_':OPERATOR
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.DATE.TIME> = DT.TM 
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.CO.CODE> = ID.COMPANY
    R.PRELOG.CREDIT.CARD.NAU<PRE.LOG.DEPT.CODE> = R.USER<EB.USE.DEPARTMENT.CODE>

    CALL F.WRITE(FN.PRELOG.CREDIT.CARD.NAU,FILE.ID,R.PRELOG.CREDIT.CARD.NAU)

    RETURN
*---------------------------------------------------------------------------------
END
