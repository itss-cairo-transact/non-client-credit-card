*-----------------------------------------------------------------------------
* <Rating>-34</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.CLEARING.UPLOAD.SELECT
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.CLEARING.UPLOAD.SELECT
*-------------------------------------------------------------------------------
*Description       : Select routine to select the records in the file placed in the path
*Linked With       : BNK/B.VISA.CLEARING.UPLOAD - batch
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*30/09/2011 - PACS00138697 - Fix
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.H.VISA.CLEARING.FILE
$INSERT I_B.VISA.CLEARING.COMMON

    GOSUB PROCESS

    RETURN
*----------------------------------------------------------------------------------
PROCESS:
* Select the file in the file path. and select the unprocessed text file
* rename it to filename.processed
*----------------------------------------------------------------------------------


    FN.FILE.OUT.PATH  = FILE.PATH
    F.FILE.OUT.PATH   = ''
    CALL OPF(FN.FILE.OUT.PATH,F.FILE.OUT.PATH)

    LINEFEED = CHAR(10)
*   SEL.STMT='SELECT ':FN.FILE.OUT.PATH:' WITH @ID EQ ...Validated'
  SEL.STMT='SELECT ':FN.FILE.OUT.PATH

  CALL EB.READLIST(SEL.STMT,SEL.LIST,'',NO.OF.REC,SEL.ERR)

    OUR.LIST = ''
    REC.NO = 1
    LOOP
REMOVE SEL.ID FROM SEL.LIST SETTING SEL.POS
    WHILE REC.NO LE NO.OF.REC

IF SEL.ID[9] EQ 'Validated' THEN

        FNAME = FIELD(SEL.LIST<REC.NO>,'.Validated',1)
        CHK.PRE.LOG.ID = "CL.":FNAME:"-":TODAY
        CALL F.READ(FN.PRELOG.CREDIT.CARD,CHK.PRE.LOG.ID,R.PRELOG.CREDIT.CARD,F.PRELOG.CREDIT.CARD,PRE.LOG.ERR)

        IF R.PRELOG.CREDIT.CARD THEN

            CALL F.READ(FN.FILE.OUT.PATH,SEL.LIST<REC.NO>,R.DATA.LIST,F.FILE.OUT.PATH,F.ERROR)
            FNAME =FIELD(SEL.LIST,'.',1)

            SEL.STMT='SELECT ':FN.H.CREDIT.CARD.LOG:' WITH FILE.NAME EQ ':FNAME
            CALL EB.READLIST(SEL.STMT,SEL.LIST.1,'',NOS,SEL.ERR)

            LOOP
                REMOVE Y.ID FROM SEL.LIST.1 SETTING POS
            WHILE Y.ID:POS
                CALL F.DELETE(FN.H.CREDIT.CARD.LOG,Y.ID)
            REPEAT

            CHANGE LINEFEED TO FM IN R.DATA.LIST

            LOOP
                REMOVE LINES FROM R.DATA.LIST SETTING LINES.POS
            WHILE LINES:LINES.POS
                OUR.LIST<-1> = LINES:'*':FNAME
            REPEAT

            FILE.NAME = FNAME:'.Processed'
            IF R.DATA.LIST THEN
                CALL F.WRITE(FN.FILE.OUT.PATH,FILE.NAME,R.DATA.LIST)
            END
            CALL F.DELETE(FN.FILE.OUT.PATH,SEL.LIST<REC.NO>)
        END

END
        REC.NO += 1
    REPEAT
    CALL BATCH.BUILD.LIST('',OUR.LIST)
    RETURN
