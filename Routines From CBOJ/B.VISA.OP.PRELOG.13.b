*-----------------------------------------------------------------------------
* <Rating>-98</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.OP.PRELOG.13
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.OP.PRELOG.13
*------------------------------------------------------------------------------
*Description       : Routine which raise the accounting entries for op code 13
*Linked With       :
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.ACCOUNT
$INSERT I_F.STMT.ENTRY
$INSERT I_F.CURRENCY
$INSERT I_F.FUNDS.TRANSFER
$INSERT I_F.USER
$INSERT I_F.H.VISA.CLEARING.FILE
$INSERT I_F.H.VISA.CLEARING.PARAM
$INSERT I_F.H.CREDIT.CARD.LOG
$INSERT I_B.VISA.CLEARING.COMMON

    GOSUB INIT
    GOSUB PROCESS

    RETURN
*----------------------------------------------------------------------------------
INIT:
*----------------------------------------------------------------------------------

    LOCATE "13" IN R.H.VISA.CLEARING.PARAM<CL.VISA.OP.CODE,1> SETTING POS THEN
        DR.TRANS.CODE   = R.H.VISA.CLEARING.PARAM<CL.DR.TRANS.CODE,POS>
        CR.TRANS.CODE   = R.H.VISA.CLEARING.PARAM<CL.CR.TRANS.CODE,POS>

        DR.NARRATIVE    = ACRONYM
        CR.NARRATIVE    = ACRONYM

*        DR.NARRATIVE    = R.H.VISA.CLEARING.PARAM<CL.DR.NARRATIVE,POS>
*        CR.NARRATIVE    = R.H.VISA.CLEARING.PARAM<CL.CR.NARRATIVE,POS>
        COND.DESC       = R.H.VISA.CLEARING.PARAM<CL.COND.DESC,POS>
        CONSTANT.NO     = R.H.VISA.CLEARING.PARAM<CL.ACCOUNT.NO,POS>
    END

    V       = FT.AUDIT.DATE.TIME
    FT.POS  = V
    FN.CURR = 'F.CURRENCY'
    F.CURR  = ''
    CALL OPF(FN.CURR,F.CURR)

    CHANGE SM TO VM IN CONSTANT.NO

    CALL F.READ(FN.ACCOUNT,CUST.ACCOUNT,R.ACCOUNT.1,F.ACCOUNT,F.ERR)
    CUST.AC.CCY = R.ACCOUNT.1<AC.CURRENCY>
    CUST.CATEG  = R.ACCOUNT.1<AC.CATEGORY>

    RETURN
*----------------------------------------------------------------------------------
ACCT.FETCH.1:
*----------------------------------------------------------------------------------

    CONSTANT.AC = CONSTANT.NO<1,1>
    CALL F.READ(FN.ACCOUNT,CONSTANT.AC,R.ACCOUNT.4,F.ACCOUNT,F.ERR)
    CONSTANT.CCY = R.ACCOUNT.4<AC.CURRENCY>
    CONSTANT.CATEG = R.ACCOUNT.4<AC.CATEGORY>

    RETURN
*----------------------------------------------------------------------------------
PROCESS:
*----------------------------------------------------------------------------------

    DR.ACC    =''
    DR.CCY    =''
    DR.CATEG  =''
    CR.ACC    =''
    CR.CCY    =''
    CR.CATEG  =''
    PL.CATEG  =''

*    SEL.CCY ='SELECT ':FN.CURR:' WITH NUMERIC.CCY.CODE EQ ':Y.TRANS.CODE
*    CALL EB.READLIST(SEL.CCY,SEL.LIST,'',NO.REC,SEL.ERR)
*    CALL F.READ(FN.CURR,SEL.LIST,R.CURR,F.CURR,F.ERR)

    CALL F.READ(FN.NUMERIC.CURRENCY,Y.TRANS.CODE,R.NUMERIC.CURRENCY,F.NUMERIC.CURRENCY,ERR.NUM.CCY)
    CALL F.READ(FN.CURR,R.NUMERIC.CURRENCY,R.CURR,F.CURR,F.ERR)

    DE.PLACE = R.CURR<EB.CUR.NO.OF.DECIMALS>
    GROSS.AMOUNT = GROSS.AMOUNT / PWR(10,DE.PLACE)
    DEBIT.AMOUNT = DEBIT.AMOUNT / PWR(10,3)
    CLEARED.AMOUNT = CLEARED.AMOUNT / PWR(10,DECI.PLACE)

    GOSUB ACCT.FETCH.1
    DR.ACC   = CLAIM.ACCOUNT
    DR.CCY   = CLAIM.AC.CCY
    DR.CATEG = CLAIM.CATEG
    AMOUNT = GROSS.AMOUNT
    DR.ACCT = DR.ACC
    GOSUB DEBIT.LEG
    CR.ACC   = CONSTANT.AC
    CR.CCY   = CONSTANT.CCY
    CR.CATEG = CONSTANT.CATEG
    AMOUNT   = GROSS.AMOUNT
    CR.ACCT = CR.ACC
    IF CR.ACC[1,2] EQ 'PL' THEN
        PL.CATEG = CR.ACC[3,7]
        CR.CATEG = PL.CATEG
        CR.ACC   = ''
        CR.CCY   = CLAIM.AC.CCY
        CR.ACCT  = 'PL':PL.CATEG
    END
    GOSUB CREDIT.LEG
    GOSUB RAISE.ENTRY

    RETURN
*----------------------------------------------------------------------------------
DEBIT.LEG:
*----------------------------------------------------------------------------------

    DR.ARRAY = ''
    DR.ARRAY<AC.STE.COMPANY.CODE>       = ID.COMPANY
    DR.ARRAY<AC.STE.CURRENCY>           = DR.CCY
    DR.ARRAY<AC.STE.TRANSACTION.CODE>   = DR.TRANS.CODE
    DR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = DR.ACC
    DR.ARRAY<AC.STE.ACCOUNT.OFFICER>    = R.USER<EB.USE.DEPARTMENT.CODE>
    DR.ARRAY<AC.STE.PRODUCT.CATEGORY>   = DR.CATEG
    DR.ARRAY<AC.STE.VALUE.DATE>         = TODAY
    DR.ARRAY<AC.STE.POSITION.TYPE>      = 'TR'
    DR.ARRAY<AC.STE.DEPARTMENT.CODE>    = R.USER<EB.USE.DEPARTMENT.CODE>
    DR.ARRAY<AC.STE.CURRENCY.MARKET>    = '1'
    DR.ARRAY<AC.STE.PL.CATEGORY>        = PL.CATEG
    DR.ARRAY<AC.STE.OUR.REFERENCE>      = DR.NARRATIVE
    DR.ARRAY<AC.STE.TRANS.REFERENCE>    = ''
    DR.ARRAY<AC.STE.SYSTEM.ID>          = 'AC'
    DR.ARRAY<AC.STE.BOOKING.DATE>       = TODAY
    DR.ARRAY<AC.STE.EXPOSURE.DATE>      = TODAY
    DR.ARRAY<AC.STE.NARRATIVE>          = DR.NARRATIVE
    CALL EB.ROUND.AMOUNT(LCCY,AMOUNT,'','')
    AMOUNT                              = ABS(AMOUNT)
    IF DR.CCY EQ LCCY THEN
        DR.ARRAY<AC.STE.AMOUNT.LCY>     = (-1) * AMOUNT
        R.H.CREDIT.CARD.LOG<CARD.LOG.DR.AMOUNT,-1> = AMOUNT
    END ELSE
        Y.CCY = DR.CCY
        GOSUB CONV.CCY
*        YR.AMT.LCY = AMOUNT
        CALL EB.ROUND.AMOUNT(LCCY,YR.AMT.LCY,'','')
*        YR.AMT.FCY = CLEARED.AMOUNT
        CALL EB.ROUND.AMOUNT(DR.CCY,YR.AMT.FCY,'','')
        DR.ARRAY<AC.STE.AMOUNT.LCY>     = (-1) * YR.AMT.LCY
        DR.ARRAY<AC.STE.AMOUNT.FCY>     = (-1) * YR.AMT.FCY
        R.H.CREDIT.CARD.LOG<CARD.LOG.DR.AMOUNT,-1> = GROSS.AMOUNT
    END
    DR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = DR.ACC
    R.H.CREDIT.CARD.LOG<CARD.LOG.VISA.DR.ACC,-1> = DR.ACCT
    MULTI.STMT<-1> = LOWER(DR.ARRAY)

    RETURN
*----------------------------------------------------------------------------------
CREDIT.LEG:
*----------------------------------------------------------------------------------

    CR.ARRAY = ''
    CR.ARRAY<AC.STE.COMPANY.CODE>       = ID.COMPANY
    CR.ARRAY<AC.STE.CURRENCY>           = CR.CCY
    CR.ARRAY<AC.STE.TRANSACTION.CODE>   = CR.TRANS.CODE
    CR.ARRAY<AC.STE.ACCOUNT.OFFICER>    = R.USER<EB.USE.DEPARTMENT.CODE>
    CR.ARRAY<AC.STE.PRODUCT.CATEGORY>   = CR.CATEG
    CR.ARRAY<AC.STE.VALUE.DATE>         = TODAY
    CR.ARRAY<AC.STE.POSITION.TYPE>      = 'TR'
    CR.ARRAY<AC.STE.DEPARTMENT.CODE>    = R.USER<EB.USE.DEPARTMENT.CODE>
    CR.ARRAY<AC.STE.CURRENCY.MARKET>    = '1'
    CR.ARRAY<AC.STE.PL.CATEGORY>        = PL.CATEG
    CR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = CR.ACC
    CR.ARRAY<AC.STE.OUR.REFERENCE>      = CR.NARRATIVE
    CR.ARRAY<AC.STE.TRANS.REFERENCE>    = ''
    CR.ARRAY<AC.STE.SYSTEM.ID>          = 'AC'
    CR.ARRAY<AC.STE.BOOKING.DATE>       = TODAY
    CR.ARRAY<AC.STE.EXPOSURE.DATE>      = TODAY
    CR.ARRAY<AC.STE.NARRATIVE>          = CR.NARRATIVE
    CALL EB.ROUND.AMOUNT(LCCY,AMOUNT,'','')
    AMOUNT                              = ABS(AMOUNT)
    IF CR.CCY EQ LCCY THEN
        CR.ARRAY<AC.STE.AMOUNT.LCY>     = AMOUNT
        R.H.CREDIT.CARD.LOG<CARD.LOG.CR.AMOUNT,-1> = AMOUNT
    END ELSE
        Y.CCY = CR.CCY
        GOSUB CONV.CCY
*        YR.AMT.LCY = AMOUNT
        CALL EB.ROUND.AMOUNT(LCCY,YR.AMT.LCY,'','')
*        YR.AMT.FCY = CLEARED.AMOUNT
        CALL EB.ROUND.AMOUNT(CR.CCY,YR.AMT.FCY,'','')
        CR.ARRAY<AC.STE.AMOUNT.LCY>     = YR.AMT.LCY
        CR.ARRAY<AC.STE.AMOUNT.FCY>     = YR.AMT.FCY
        R.H.CREDIT.CARD.LOG<CARD.LOG.CR.AMOUNT,-1> = GROSS.AMOUNT
    END

    MULTI.STMT<-1> = LOWER(CR.ARRAY)
    R.H.CREDIT.CARD.LOG<CARD.LOG.VISA.CR.ACC,-1> = CR.ACCT

    RETURN
*-----------------------------------------------------------------------------
CONV.CCY:
*-----------------------------------------------------------------------------

    BUY.CCY   = Y.CCY
    SELL.CCY  = LCCY
    CCY.MKT   = 1
    BUY.AMT   = ABS(AMOUNT)
    SELL.AMT  = '' ; BASE.CCY = ''
    DIFF = '' ; LCY.AMT = '' ; ERR = '' ; RATE = ''
    CALL EXCHRATE(CCY.MKT,BUY.CCY,BUY.AMT,SELL.CCY,SELL.AMT,BASE.CCY,RATE,DIFF,LCY.AMT,ERR)
    YR.AMT.LCY = LCY.AMT
    YR.AMT.FCY = AMOUNT
    YR.RATE    = RATE

    RETURN

*----------------------------------------------------------------------------------
RAISE.ENTRY:
*----------------------------------------------------------------------------------

    ID.NEW = TXN.ID
    SYSTEM.ID = 'AC'
    TYPE = 'SAO'
    ENTRY.ARRAY = MULTI.STMT
    ACCOUNTING.TYPE  = ""
    ERR.REC = ""
    CALL EB.ACCOUNTING.CHECK(SYSTEM.ID,TYPE,ENTRY.ARRAY,ACCOUNTING.TYPE,ERR.REC)

    IF ERR.REC EQ '' THEN
        TYPE ='VAL'
        CALL EB.ACCOUNTING.CHECK(SYSTEM.ID,TYPE,ENTRY.ARRAY,ACCOUNTING.TYPE,ERR.REC)
        V = V - 9
        OVR.MSG.ALL = R.NEW(V)
        R.NEW(V) =''
        OVR.MSG.CNT = DCOUNT(OVR.MSG.ALL,@VM)
        TYPE ='DEL'
        CALL EB.ACCOUNTING.CHECK(SYSTEM.ID,TYPE,ENTRY.ARRAY,ACCOUNTING.TYPE,ERR.REC)
    END
    R.H.CREDIT.CARD.LOG<CARD.LOG.OVR.MSG> = ''

    IF OVR.MSG.CNT THEN
        CNT=0
        LOOP
            CNT+=1
        WHILE CNT LE OVR.MSG.CNT
            OVR.MSG = OVR.MSG.ALL<1,CNT>
            GOSUB PROCESS.OVERRIDE
        REPEAT
    END

    GOSUB CARD.LOG

    RETURN
*-----------------------------------------------------------------------------
CARD.LOG:
*-----------------------------------------------------------------------------

    R.H.CREDIT.CARD.LOG<CARD.LOG.CARD.TYPE>        = 'VISA'
    R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.NAME>        = FNAME
    R.H.CREDIT.CARD.LOG<CARD.LOG.FILE.TYPE>        = 'Clearing_File'
    R.H.CREDIT.CARD.LOG<CARD.LOG.TRANSACTION.REF>  = TXN.ID
    IF ERR.REC NE '' THEN
        R.H.CREDIT.CARD.LOG<CARD.LOG.TRANS.STATUS> = 'FAILURE'
        R.H.CREDIT.CARD.LOG<CARD.LOG.ERR.MSG>      = ERR.REC
    END ELSE
        STMT.POS = FT.POS - 10
*        R.H.CREDIT.CARD.LOG<CARD.LOG.STMT.ID>      = R.NEW(STMT.POS)
        R.H.CREDIT.CARD.LOG<CARD.LOG.TRANS.STATUS> = 'SUCCESS'
    END
    R.H.CREDIT.CARD.LOG<CARD.LOG.DATE>             = TODAY
    R.H.CREDIT.CARD.LOG<CARD.LOG.NARRATIVE>        = ACRONYM
    R.H.CREDIT.CARD.LOG<CARD.LOG.MARKER>           = "Validated"

    CALL ALLOCATE.UNIQUE.TIME(UNIQUE.TIME)
    UNIQUE.TIME = DATE():UNIQUE.TIME
    CREDIT.LOG.ID = UNIQUE.TIME

    CALL F.WRITE(FN.H.CREDIT.CARD.LOG,CREDIT.LOG.ID,R.H.CREDIT.CARD.LOG)

    RETURN
*-----------------------------------------------------------------------------
PROCESS.OVERRIDE:
*----------------
    TEXT = ''
    CNT.VAL = COUNT(OVR.MSG,'&')
    VAL = FIELD(OVR.MSG,"}",1)
    VAL.1 = FIELD(OVR.MSG,VAL,2)
    VAL.2 = FIELD(FIELD(VAL.1,"{",1),"}",2)
    VAL.3 = FIELD(FIELD(OVR.MSG,VAL.2,2),"{",2)
    TEXT = VAL.2
    FOR STA.VAL = 1 TO CNT.VAL
        TEXT<2,STA.VAL> = FIELD(VAL.3,"}",STA.VAL)
    NEXT STA.VAL
    CALL TXT(TEXT)
    IF VAL NE 'POSTING.RESTRICT' THEN
        R.H.CREDIT.CARD.LOG<CARD.LOG.OVR.MSG,-1> = TEXT
    END ELSE
        IF ER.POS.RES EQ "YES" THEN
            R.H.CREDIT.CARD.LOG<CARD.LOG.ERR.MSG,-1> = TEXT
        END ELSE
            R.H.CREDIT.CARD.LOG<CARD.LOG.OVR.MSG,-1> = TEXT
        END
    END
    RETURN
*---------------------------------------------------------------------------------------
END
