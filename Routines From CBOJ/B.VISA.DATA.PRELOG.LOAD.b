*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.DATA.PRELOG.LOAD
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.DATA.PRELOG.LOAD
*-------------------------------------------------------------------------------
*Description       : This load routine which loads the parameter file and store variables
*                       in common file
*Linked With       : BNK/B.VISA.DATA.PRELOG
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198(Parent ODR),ODR-2011-02-0055
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.H.VISA.PAYMENT.FILE
    $INSERT I_F.H.UTILITY.FORMAT.PRINT
    $INSERT I_F.H.VISA.DATA.PARAM
    $INSERT I_F.H.CREDIT.CARD.LOG
    $INSERT I_F.ACCOUNT
    $INSERT I_F.ALTERNATE.ACCOUNT
    $INSERT I_F.STMT.ENTRY
    $INSERT I_F.CURRENCY
    $INSERT I_B.VISA.DATA.COMMON
    $INSERT I_F.CARD.ISSUE
    $INSERT I_F.NUMERIC.CURRENCY

    GOSUB INIT
    GOSUB OPENFILE
    GOSUB PROCESS

    RETURN
*----------------------------------------------------------------------------------
INIT:
*----------------------------------------------------------------------------------
    FN.H.VISA.PAYMENT.FILE = 'F.H.VISA.PAYMENT.FILE'
    F.H.VISA.PAYMENT.FILE = ''

    FN.ACCOUNT ='F.ACCOUNT'
    F.ACCOUNT =''

    FN.H.CREDIT.CARD.LOG ='F.H.CREDIT.CARD.LOG'
    F.H.CREDIT.CARD.LOG =''

    FN.H.UTILITY.FORMAT.PRINT = 'F.H.UTILITY.FORMAT.PRINT'
    F.H.UTILITY.FORMAT.PRINT =''

    FN.H.VISA.DATA.PARAM='F.H.VISA.DATA.PARAM'
* F.H.VISA.DATA.PARAM=''    ;*TUS-S/E

    FN.CURR='F.CURRENCY'
    F.CURR =''

    FN.CARD.ISSUE='F.CARD.ISSUE'
    F.CARD.ISSUE=''

    FN.ALTERNATE.ACCOUNT='F.ALTERNATE.ACCOUNT'
    F.ALTERNATE.ACCOUNT =''

    FN.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
    F.NUMERIC.CURRENCY = ''

    FN.CARD.ISSUE.ACCOUNT = 'F.CARD.ISSUE.ACCOUNT'
    F.CARD.ISSUE.ACCOUNT = ''

    RETURN
*----------------------------------------------------------------------------------
OPENFILE:
*----------------------------------------------------------------------------------
    CALL OPF(FN.CARD.ISSUE.ACCOUNT,F.CARD.ISSUE.ACCOUNT)
    CALL OPF(FN.NUMERIC.CURRENCY,F.NUMERIC.CURRENCY)
    CALL OPF(FN.H.VISA.PAYMENT.FILE,F.H.VISA.PAYMENT.FILE)
    CALL OPF(FN.H.CREDIT.CARD.LOG,F.H.CREDIT.CARD.LOG)
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)
    CALL OPF(FN.H.UTILITY.FORMAT.PRINT,F.H.UTILITY.FORMAT.PRINT)
*  CALL OPF(FN.H.VISA.DATA.PARAM,F.H.VISA.DATA.PARAM)  ;*TUS-S/E
    CALL OPF(FN.CURR,F.CURR)
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
    CALL OPF(FN.ALTERNATE.ACCOUNT,F.ALTERNATE.ACCOUNT)

    RETURN
*----------------------------------------------------------------------------------
PROCESS:
*----------------------------------------------------------------------------------
    CALL F.READ(FN.H.UTILITY.FORMAT.PRINT,'VISA.PAYMENT.FILE',R.H.UTILITY.FORMAT.PRINT,F.H.UTILITY.FORMAT.PRINT,F.ERROR)
    FIELD.NAME   = R.H.UTILITY.FORMAT.PRINT<UF.FIELD.NAME>
    POSITION.ARR = R.H.UTILITY.FORMAT.PRINT<UF.ONLINE.FMT>
    LENGTH.ARR   = R.H.UTILITY.FORMAT.PRINT<UF.DIRECT.DEBIT.FMT>

    CALL CACHE.READ(FN.H.VISA.DATA.PARAM,'SYSTEM',R.VISA.DATA.PARAM,F.ERROR)
    FILE.PATH         = R.VISA.DATA.PARAM<DP.FILE.PATH>
    DR.TRANS.CODE     = R.VISA.DATA.PARAM<DP.DR.TRANS.CODE>
    CR.TRANS.CODE     = R.VISA.DATA.PARAM<DP.CR.TRANS.CODE>
    DR.NARRATIVE      = R.VISA.DATA.PARAM<DP.DR.NARRATIVE>
    CR.NARRATIVE      = R.VISA.DATA.PARAM<DP.CR.NARRATIVE>
    ARREAR.CATEG      = R.VISA.DATA.PARAM<DP.ARREAR.AC.CATEG>
    CLAIM.CATEG       = R.VISA.DATA.PARAM<DP.CLAIM.AC.CATEG>
    ER.POS.RES        = R.VISA.DATA.PARAM<DP.ERR.POST.REST>
    FN.FILE.OUT.PATH  = FILE.PATH
    F.FILE.OUT.PATH   = ''
    CALL OPF(FN.FILE.OUT.PATH,F.FILE.OUT.PATH)

    RETURN
