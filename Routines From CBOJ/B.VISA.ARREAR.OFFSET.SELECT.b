*-----------------------------------------------------------------------------
* <Rating>-34</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.ARREAR.OFFSET.SELECT
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.ARREAR.OFFSET.SELECT
*-------------------------------------------------------------------------------
*Description       : Select the card issue file
*Linked With       : BNK/B.VISA.ARREAR.OFFSET.SELECT
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
* 28/02/2012 - PACS00183889
* 17/04/2012 - PACS00191480 - Dhinesh - no changes in this routine. Changes done in Load and mainline routine.
* 5/04/2013  - PACS00249399 - offset arrear settlement takking cancelled card account
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.CARD.ISSUE
$INSERT I_F.ACCOUNT
$INSERT I_B.VISA.ARREAR.COMMON

    GOSUB PROCESS

    RETURN

PROCESS:
* Select all the Cards(Visa and Master)
    ARR.ACC.LIST = ''

*PACS00249399 - S
*    SEL.STMT = 'SELECT ':FN.CARD.ISSUE:' WITH @ID LIKE VISA... OR @ID LIKE MAST...'
    SEL.STMT = 'SELECT ':FN.CARD.ISSUE:' WITH (@ID LIKE VISA... OR @ID LIKE MAST...) AND CARD.STATUS EQ 90'
*PACS00249399 - E

    CALL EB.READLIST(SEL.STMT,SEL.LIST,'',NO.OF.REC,SEL.ERR)
    CNT = 1
    LOOP
    WHILE CNT LE NO.OF.REC
        R.CARD.ISSUE = ''
        CARD.ID = SEL.LIST<CNT>

        CALL F.READ(FN.CARD.ISSUE,CARD.ID,R.CARD.ISSUE,F.CARD.ISSUE,CARD.ERR)
        GOSUB CHECK.CREDIT.CARDS
        CNT = CNT + 1
    REPEAT

    CALL BATCH.BUILD.LIST('',FINAL.SEL.LIST)

    RETURN

CHECK.CREDIT.CARDS:
******************
    ACCOUNT.LIST = R.CARD.ISSUE<CARD.IS.ACCOUNT>
    CNT.ACC = DCOUNT(ACCOUNT.LIST,VM)

    IF FIELD(CARD.ID,'.',1) EQ 'MAST' THEN
        ARR.CATG = MAST.ARREAR.CATEG
    END ELSE
        ARR.CATG = ARREAR.CATEG
    END
    CNT.LOOP = 1
    LOOP
    WHILE CNT.LOOP LE CNT.ACC
        ACC.ID = ACCOUNT.LIST<1,CNT.LOOP>
        CALL F.READ(FN.ACCOUNT,ACC.ID,R.ACCOUNT,F.ACCOUNT,ACC.ERR)
        IF R.ACCOUNT<AC.CATEGORY> MATCHES ARR.CATG THEN
            LOCATE ACC.ID IN ARR.ACC.LIST SETTING ACC.ACC.POS ELSE
                ARR.ACC.LIST<-1> = ACC.ID
                FINAL.SEL.LIST<-1> = CARD.ID
            END
        END
        CNT.LOOP = CNT.LOOP + 1
    REPEAT

    RETURN

END
