*-----------------------------------------------------------------------------
* <Rating>-70</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.DATA.UPLOAD(Y.LINE)
*-----------------------------------------------------------------------------
*   Routine Name : B.VISA.TERM.UPLOAD
*   Description  : This record routine which raise the accounting entries
*                  for the visa term file
*
*   Developed By : TAM
*   ODR Details  : ODR-2010-06-0198
*   Type         : BATCH ROUINE
*-----------------------------------------------------------------------------
*
*
*
*-----------------------------------------------------------------------------

$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.STMT.ENTRY
$INSERT I_F.FUNDS.TRANSFER
$INSERT I_F.H.VISA.PAYMENT.FILE
$INSERT I_F.H.UTILITY.FORMAT.PRINT
$INSERT I_F.H.VISA.DATA.PARAM
$INSERT I_F.H.CREDIT.CARD.LOG
$INSERT I_F.ACCOUNT
$INSERT I_F.ALTERNATE.ACCOUNT
$INSERT I_F.CURRENCY
$INSERT I_F.USER
$INSERT I_F.CARD.ISSUE
$INSERT I_B.VISA.DATA.COMMON


    GOSUB PROCESS

    RETURN
*-----------------------------------------------------------------------------
PROCESS:
* Read the Account category parameter table and get the corresponding account
* and get the length, positions from the H.UTILITY.FORMAT.PRINT- VISA.TERM.FILE
*-----------------------------------------------------------------------------
*Nov20th-Dhinesh
    DELIMIT.VALUE = DCOUNT(Y.LINE,'*')
    FNAME = FIELD(Y.LINE,'*',DELIMIT.VALUE)
    Y.LINE = FIELD(Y.LINE,'*',1,DELIMIT.VALUE-1)

    FD.CNT  = DCOUNT(FIELD.NAME,VM)
    V       = FT.AUDIT.DATE.TIME
    FT.POS  = V

    FOR INT.VAR = 1 TO FD.CNT
        R.H.VISA.PAYMENT.FILE<-1> = Y.LINE[POSITION.ARR<1,INT.VAR>,LENGTH.ARR<1,INT.VAR>]
    NEXT INT.VAR

    CALL ALLOCATE.UNIQUE.TIME(UNIQUE.TIME)
    UNIQUE.TIME  = DATE():UNIQUE.TIME
    TXN.ID       = 'VISA':UNIQUE.TIME

    GOSUB ACCT.FETCH

    CALL F.WRITE(FN.H.VISA.PAYMENT.FILE,TXN.ID,R.H.VISA.PAYMENT.FILE)
    GOSUB CLEAR.VARIABLES

    RETURN
*-----------------------------------------------------------------------------
CLEAR.VARIABLES:
*-----------------------------------------------------------------------------

    CR.ACCT.NO = '' ; AMOUNT = '' ; CCY.CODE= '' ;ACCT.ID = '' ;
    DR.ACCT.NO = '' ; DR.CATEG = '' ; DR.CCY ='' ;


    RETURN
*-----------------------------------------------------------------------------
ACCT.FETCH:
* MXP Account Number and  Transaction Amount from the Incoming file
*-----------------------------------------------------------------------------

    LOCATE "VP.MXP.ACC.NO" IN FIELD.NAME<1,1> SETTING Y.POS THEN
        START.POS   = R.H.UTILITY.FORMAT.PRINT<UF.ONLINE.FMT,Y.POS>
        END.POS     = R.H.UTILITY.FORMAT.PRINT<UF.DIRECT.DEBIT.FMT,Y.POS>
        CR.ACCT.NO  = Y.LINE[START.POS,END.POS]
        CR.ACCT.NO  = TRIM(CR.ACCT.NO," ","D")
    END
    LOCATE "VP.MINI.DUE.AMT" IN FIELD.NAME<1,1> SETTING Y.POS THEN
        START.POS   = R.H.UTILITY.FORMAT.PRINT<UF.ONLINE.FMT,Y.POS>
        END.POS     = R.H.UTILITY.FORMAT.PRINT<UF.DIRECT.DEBIT.FMT,Y.POS>
        AMOUNT      = TRIM(Y.LINE[START.POS,END.POS],"0","L")
    END
    LOCATE "VP.CURRENCY.CODE" IN FIELD.NAME<1,1> SETTING Y.POS THEN
        START.POS   = R.H.UTILITY.FORMAT.PRINT<UF.ONLINE.FMT,Y.POS>
        END.POS     = R.H.UTILITY.FORMAT.PRINT<UF.DIRECT.DEBIT.FMT,Y.POS>
        CCY.CODE    = Y.LINE[START.POS,END.POS]
    END
    CALL F.READ(FN.ALTERNATE.ACCOUNT,CR.ACCT.NO,R.ALTERNATE.ACCOUNT,F.ALTERNATE.ACCOUNT,F.ERROR)
    IF R.ALTERNATE.ACCOUNT THEN
        CR.ACCT.NO = R.ALTERNATE.ACCOUNT<1>
    END

*    SELECT.STMT = 'SELECT ':FN.CARD.ISSUE:' WITH ACCOUNT EQ ':CR.ACCT.NO
*    CALL EB.READLIST(SELECT.STMT,SELECT.LIST,'',NO.OF.REC,SL.ERR)
* To get the card number from CARD.ISSUE.ACCOUNT
    CALL F.READ(FN.CARD.ISSUE.ACCOUNT,CR.ACCT.NO,R.CARD.ISSUE.ACCOUNT,F.CARD.ISSUE.ACCOUNT,ERR.CARD.ISS)

    LOOP
        REMOVE GET.CARD.NO FROM R.CARD.ISSUE.ACCOUNT SETTING CARD.ISS.POS
    WHILE GET.CARD.NO:CARD.ISS.POS
        IF GET.CARD.NO[1,4] EQ 'VISA' THEN
            VISA.CARD.NO = GET.CARD.NO
        END
    REPEAT

    CALL F.READ(FN.CARD.ISSUE,VISA.CARD.NO,R.CARD.ISSUE,F.CARD.ISSUE,F.ERROR)

    AC.COUNT = DCOUNT(R.CARD.ISSUE<CARD.IS.ACCOUNT>,VM)
    CNT=0

    LOOP
        CNT+=1
    WHILE CNT LE AC.COUNT
        ACCT.ID = R.CARD.ISSUE<CARD.IS.ACCOUNT,CNT>
        CALL F.READ(FN.ACCOUNT,ACCT.ID,R.ACCT,F.ACCOUNT,F.ERR)
        *IF ARREAR.CATEG EQ R.ACCT<AC.CATEGORY> THEN
		IF R.ACCT<AC.CATEGORY> MATCHES ARREAR.CATEG THEN
            DR.ACCT.NO  = ACCT.ID
            DR.CATEG    = R.ACCT<AC.CATEGORY>
            DR.CCY      = R.ACCT<AC.CURRENCY>
            DR.ACCT.OFF = R.ACCT<AC.ACCOUNT.OFFICER>
        END
    REPEAT

    ERR.REC = ''
    CALL F.READ(FN.ACCOUNT,CR.ACCT.NO,R.ACCOUNT,F.ACCOUNT,F.ERROR)
    IF R.ACCOUNT THEN
        CR.CCY      = R.ACCOUNT<AC.CURRENCY>
        CR.CATEG    = R.ACCOUNT<AC.CATEGORY>
        CR.ACCT.OFF = R.ACCOUNT<AC.ACCOUNT.OFFICER>
    END ELSE
        ERR.REC = "INVALID ACCOUNT NUMBER"
        GOSUB CARD.LOG
*GOSUB PGM.END
        RETURN
    END

*    SEL.ST = 'SELECT ':FN.CURR:' WITH NUMERIC.CCY.CODE EQ ':CCY.CODE
*    CALL EB.READLIST(SEL.ST,SL.LIST,'',NO.REC,SL.ERR)

    CALL F.READ(FN.NUMERIC.CURRENCY,CCY.CODE,R.NUMERIC.CURRENCY,F.NUMERIC.CURRENCY,ERR.NUM.CCY)
    CALL F.READ(FN.CURR,R.NUMERIC.CURRENCY,R.CURR,F.CURR,F.ERR)
    DECI.PLACE = R.CURR<EB.CUR.NO.OF.DECIMALS>
    AMOUNT = AMOUNT / PWR(10,DECI.PLACE)

    GOSUB ACCOUNT.ENTRIES
    RETURN
*-----------------------------------------------------------------------------
ACCOUNT.ENTRIES:
* Generating the Accounting entries using EB.ACCOUNTING
*-----------------------------------------------------------------------------
    DR.ARRAY =''
    DR.ARRAY<AC.STE.COMPANY.CODE>       = ID.COMPANY
    DR.ARRAY<AC.STE.CURRENCY>           = DR.CCY
    DR.ARRAY<AC.STE.TRANSACTION.CODE>   = DR.TRANS.CODE
    DR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = DR.ACCT.NO
    DR.ARRAY<AC.STE.ACCOUNT.OFFICER>    = R.USER<EB.USE.DEPARTMENT.CODE>
    DR.ARRAY<AC.STE.PRODUCT.CATEGORY>   = DR.CATEG
    DR.ARRAY<AC.STE.VALUE.DATE>         = TODAY
    DR.ARRAY<AC.STE.POSITION.TYPE>      = 'TR'
    DR.ARRAY<AC.STE.DEPARTMENT.CODE>    = R.USER<EB.USE.DEPARTMENT.CODE>
    DR.ARRAY<AC.STE.CURRENCY.MARKET>    = '1'
    DR.ARRAY<AC.STE.PL.CATEGORY>        = ''
    DR.ARRAY<AC.STE.OUR.REFERENCE>      = DR.NARRATIVE
    DR.ARRAY<AC.STE.TRANS.REFERENCE>    = ''
    DR.ARRAY<AC.STE.SYSTEM.ID>          = 'AC'
    DR.ARRAY<AC.STE.BOOKING.DATE>       = TODAY
    DR.ARRAY<AC.STE.EXPOSURE.DATE>      = TODAY
    DR.ARRAY<AC.STE.NARRATIVE>          = DR.NARRATIVE

    IF DR.CCY EQ LCCY THEN
        DR.ARRAY<AC.STE.AMOUNT.LCY>     = (-1) * AMOUNT
        R.CREDIT.CARD.LOG<CARD.LOG.DR.AMOUNT,-1> = AMOUNT
    END ELSE
        Y.CCY = DR.CCY
        GOSUB CONV.CCY
        DR.ARRAY<AC.STE.AMOUNT.LCY>     = (-1) * YR.AMT.LCY
        DR.ARRAY<AC.STE.AMOUNT.FCY>     = (-1) * YR.AMT.FCY
        R.CREDIT.CARD.LOG<CARD.LOG.DR.AMOUNT,-1> = YR.AMT.FCY
    END
    DR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = DR.ACCT.NO
    R.CREDIT.CARD.LOG<CARD.LOG.VISA.DR.ACC,-1> = DR.ACCT.NO

    MULTI.STMT<-1> = LOWER(DR.ARRAY)

    CR.ARRAY = ''
    CR.ARRAY<AC.STE.COMPANY.CODE>       = ID.COMPANY
    CR.ARRAY<AC.STE.CURRENCY>           = CR.CCY
    CR.ARRAY<AC.STE.TRANSACTION.CODE>   = CR.TRANS.CODE
    CR.ARRAY<AC.STE.ACCOUNT.OFFICER>    = R.USER<EB.USE.DEPARTMENT.CODE>
    CR.ARRAY<AC.STE.PRODUCT.CATEGORY>   = CR.CATEG
    CR.ARRAY<AC.STE.VALUE.DATE>         = TODAY
    CR.ARRAY<AC.STE.POSITION.TYPE>      = 'TR'
    CR.ARRAY<AC.STE.DEPARTMENT.CODE>    = R.USER<EB.USE.DEPARTMENT.CODE>
    CR.ARRAY<AC.STE.CURRENCY.MARKET>    = '1'
    CR.ARRAY<AC.STE.PL.CATEGORY>        = ''
    CR.ARRAY<AC.STE.ACCOUNT.NUMBER>     = CR.ACCT.NO
    CR.ARRAY<AC.STE.OUR.REFERENCE>      = CR.NARRATIVE
    CR.ARRAY<AC.STE.TRANS.REFERENCE>    = ''
    CR.ARRAY<AC.STE.SYSTEM.ID>          = 'AC'
    CR.ARRAY<AC.STE.BOOKING.DATE>       = TODAY
    CR.ARRAY<AC.STE.EXPOSURE.DATE>      = TODAY
    CR.ARRAY<AC.STE.NARRATIVE>          = CR.NARRATIVE

    IF CR.CCY EQ LCCY THEN
        CR.ARRAY<AC.STE.AMOUNT.LCY>     = AMOUNT
        R.CREDIT.CARD.LOG<CARD.LOG.CR.AMOUNT,-1> = AMOUNT
    END ELSE
        Y.CCY = CR.CCY
        GOSUB CONV.CCY
        CR.ARRAY<AC.STE.AMOUNT.LCY>     = YR.AMT.LCY
        CR.ARRAY<AC.STE.AMOUNT.FCY>     = YR.AMT.FCY
        R.CREDIT.CARD.LOG<CARD.LOG.CR.AMOUNT,-1> = YR.AMT.FCY
    END
    R.CREDIT.CARD.LOG<CARD.LOG.VISA.CR.ACC,-1> = CR.ACCT.NO
    MULTI.STMT<-1>  = LOWER(CR.ARRAY)
    ID.NEW          = TXN.ID
    SYSTEM.ID       = 'AC'
    TYPE            = 'SAO'
    ENTRY.ARRAY     = MULTI.STMT
    ACCOUNTING.TYPE = ""
    ERR.REC         = ""
    CALL EB.ACCOUNTING.CHECK(SYSTEM.ID,TYPE,ENTRY.ARRAY,ACCOUNTING.TYPE,ERR.REC)

    IF ERR.REC EQ '' THEN
        CALL EB.ACCOUNTING.WRAPPER(SYSTEM.ID,TYPE,ENTRY.ARRAY,ACCOUNTING.TYPE,ERR.REC)
    END

    GOSUB CARD.LOG
    RETURN
*-----------------------------------------------------------------------------
CONV.CCY:
*-----------------------------------------------------------------------------
    BUY.CCY   = Y.CCY
    SELL.CCY  = LCCY
    CCY.MKT   = 1
    BUY.AMT   = ABS(AMOUNT)
    SELL.AMT  = '' ; BASE.CCY = ''
    DIFF = '' ; LCY.AMT = '' ; ERR = '' ; RATE = ''
    CALL EXCHRATE(CCY.MKT,BUY.CCY,BUY.AMT,SELL.CCY,SELL.AMT,BASE.CCY,RATE,DIFF,LCY.AMT,ERR)
    YR.AMT.LCY = LCY.AMT
    YR.AMT.FCY = AMOUNT
    YR.RATE    = RATE

    RETURN

*-----------------------------------------------------------------------------
CARD.LOG:
*-----------------------------------------------------------------------------

    R.CREDIT.CARD.LOG<CARD.LOG.CARD.TYPE>        = 'VISA'
    R.CREDIT.CARD.LOG<CARD.LOG.FILE.NAME>        = FNAME
    R.CREDIT.CARD.LOG<CARD.LOG.FILE.TYPE>        = 'Term_Data_File'
    R.CREDIT.CARD.LOG<CARD.LOG.TRANSACTION.REF>  = TXN.ID
    R.CREDIT.CARD.LOG<CARD.LOG.NARRATIVE>        = 'Due Payment for the Month-':TODAY[5,2]
    IF ERR.REC NE '' THEN
        R.CREDIT.CARD.LOG<CARD.LOG.TRANS.STATUS> = 'FAILURE'
        R.CREDIT.CARD.LOG<CARD.LOG.ERR.MSG>      = ERR.REC
    END ELSE
        STMT.POS = FT.POS - 10
        R.CREDIT.CARD.LOG<CARD.LOG.STMT.ID>      = R.NEW(STMT.POS)
        R.CREDIT.CARD.LOG<CARD.LOG.TRANS.STATUS> = 'SUCCESS'
    END
    R.CREDIT.CARD.LOG<CARD.LOG.DATE>             = TODAY

    CALL ALLOCATE.UNIQUE.TIME(UNIQUE.TIME)
    UNIQUE.TIME = DATE():UNIQUE.TIME
    CREDIT.LOG.ID = UNIQUE.TIME

    CALL F.WRITE(FN.H.CREDIT.CARD.LOG,CREDIT.LOG.ID,R.CREDIT.CARD.LOG)

    RETURN
*-----------------------------------------------------------------------------

END
