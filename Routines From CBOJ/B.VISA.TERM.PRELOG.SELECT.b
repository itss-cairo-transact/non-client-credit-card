*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.TERM.PRELOG.SELECT
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.TERM.UPLOAD.SELECT
*-------------------------------------------------------------------------------
*Description       : This routine which select the file from the path mentioned
*Linked With       : BNK/B.VISA.TERM.UPLOAD
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198-ParentODR, CR-ODR-2011-02-0055
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.H.VISA.TERM.FILE
$INSERT I_B.VISA.TERM.COMMON

    GOSUB PROCESS

    RETURN

PROCESS:

    LINEFEED = CHAR(10)
*   SEL.STMT='SELECT ':FN.FILE.OUT.PATH:' WITH @ID UNLIKE ...Processed AND @ID UNLIKE ...Validated'
    SEL.STMT='SELECT ':FN.FILE.OUT.PATH
    CALL EB.READLIST(SEL.STMT,SEL.LIST,'',NO.OF.REC,SEL.ERR)

    LOOP

	REMOVE CURR.SEL.ID FROM SEL.LIST SETTING CURR.POS
    WHILE CURR.SEL.ID NE ''

IF CURR.SEL.ID[9] NE 'Processed' AND CURR.SEL.ID[9] NE 'Validated' THEN

    FNAME = CURR.SEL.ID
    CALL F.READ(FN.FILE.OUT.PATH,FNAME,R.DATA.LIST,F.FILE.OUT.PATH,F.ERROR)

    CHANGE LINEFEED TO FM IN R.DATA.LIST

    SEL.STMT='SELECT ':FN.H.CREDIT.CARD.LOG:' WITH FILE.NAME EQ ':FNAME:' AND MARKER EQ ':"Validated"
    CALL EB.READLIST(SEL.STMT,SEL.LIST.1,'',NOS,SEL.ERR)

    LOOP
        REMOVE Y.ID FROM SEL.LIST.1 SETTING POS
    WHILE Y.ID:POS
        CALL F.DELETE(FN.H.CREDIT.CARD.LOG,Y.ID)
    REPEAT

    LOOP
        REMOVE LINES FROM R.DATA.LIST SETTING LINES.POS
    WHILE LINES:LINES.POS
        OUR.LIST<-1> = LINES:'#*#':FNAME
    REPEAT

    CALL BATCH.BUILD.LIST('',OUR.LIST)

    FILE.NAME = FNAME:'.Validated'
    IF R.DATA.LIST THEN
        CALL F.WRITE(FN.FILE.OUT.PATH,FILE.NAME,R.DATA.LIST)
    END
    CALL F.DELETE(FN.FILE.OUT.PATH,SEL.LIST<1,1>)

END

REPEAT

    RETURN

END
