*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.DATA.PRELOG.SELECT
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.DATA.PRELOG.SELECT
*-------------------------------------------------------------------------------
*Description       :Select routine which selects all the records from the file
*Linked With       : BATCH - BNK/B.VISA.DATA.PRELOG
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198-Parent ODR, ODR-2011-02-0055
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.H.VISA.PAYMENT.FILE
    $INSERT I_B.VISA.DATA.COMMON

    GOSUB PROCESS

    RETURN
*--------------------------------------------------------------------------------
PROCESS:
*-------------------------------------------------------------------------------
    LINEFEED = CHAR(10)
*   SEL.STMT='SELECT ':FN.FILE.OUT.PATH:' WITH @ID UNLIKE ...Processed AND @ID UNLIKE ...Validated'
    SEL.STMT='SELECT ':FN.FILE.OUT.PATH
    CALL EB.READLIST(SEL.STMT,SEL.LIST,'',NO.OF.REC,SEL.ERR)

    LOOP

	REMOVE CURR.SEL.ID FROM SEL.LIST SETTING CURR.POS
    WHILE CURR.SEL.ID NE ''

IF CURR.SEL.ID[9] NE 'Processed' AND CURR.SEL.ID[9] NE 'Validated' THEN

    FNAME = CURR.SEL.ID


    CALL F.READ(FN.FILE.OUT.PATH,FNAME,R.DATA.LIST,F.FILE.OUT.PATH,F.ERROR)

    SEL.STMT='SELECT ':FN.H.CREDIT.CARD.LOG:' WITH FILE.NAME EQ ':FNAME:' AND MARKER EQ ':"Validated"
    CALL EB.READLIST(SEL.STMT,SEL.LIST.1,'',NOS,SEL.ERR)

    LOOP
        REMOVE Y.ID FROM SEL.LIST.1 SETTING POS
    WHILE Y.ID:POS
        CALL F.DELETE(FN.H.CREDIT.CARD.LOG,Y.ID)
    REPEAT

    CHANGE LINEFEED TO FM IN R.DATA.LIST
    LOOP
        REMOVE LINES FROM R.DATA.LIST SETTING LINES.POS
    WHILE LINES:LINES.POS
        OUR.LIST<-1> = LINES:'*':FNAME
    REPEAT

    CALL BATCH.BUILD.LIST('',OUR.LIST)

    FILE.NAME = FNAME:'.Validated'
    IF R.DATA.LIST THEN
        CALL F.WRITE(FN.FILE.OUT.PATH,FILE.NAME,R.DATA.LIST)
    END
    CALL F.DELETE(FN.FILE.OUT.PATH,FNAME)

END

REPEAT

    RETURN
