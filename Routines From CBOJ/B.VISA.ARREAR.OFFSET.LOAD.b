*-----------------------------------------------------------------------------
* <Rating>-42</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE B.VISA.ARREAR.OFFSET.LOAD
*-----------------------------------------------------------------------------
*Company Name : Capital Bank Of Jordan
*Developed By : TAM
*Program Name : B.VISA.ARREAR.OFFSET.LOAD
*-------------------------------------------------------------------------------
*Description : Load the parameter file and store it common file
*Linked With : BNK/B.VISA.ARREAR.OFFSET.LOAD
*In Parameter : NULL
*Out Parameter : NULL
*ODR Number : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
* 17/04/2012 - PACS00191480 - Dhinesh
*
*---------------------------------------------------------------------------------
$INSERT I_EQUATE
$INSERT I_COMMON
$INSERT I_F.CARD.ISSUE
$INSERT I_F.DATES
$INSERT I_F.H.VISA.BATCH.PARAMETER
$INSERT I_B.VISA.ARREAR.COMMON
$INSERT I_F.H.LOCAL.PARAMETER
$INSERT I_F.H.CARD.ACCOUNT.CATEGORY

GOSUB INIT
GOSUB OPEN.FILE
GOSUB PROCESS

RETURN
*---------------------------------------------------------------------------------
INIT:
*---------------------------------------------------------------------------------
FN.CARD.ISSUE='F.CARD.ISSUE'
F.CARD.ISSUE=''

FN.H.VISA.BATCH.PARAMETER ='F.H.VISA.BATCH.PARAMETER'
*F.H.VISA.BATCH.PARAMETER =''  ;*TUS S/E

FN.ACCOUNT ='F.ACCOUNT'
F.ACCOUNT=''

FN.H.LOCAL.PARAMETER='F.H.LOCAL.PARAMETER'
*F.H.LOCAL.PARAMETER=''  ;*TUS S/E

FN.H.CARD.ACCOUNT.CATEGORY = 'F.H.CARD.ACCOUNT.CATEGORY'
*F.H.CARD.ACCOUNT.CATEGORY =''  ;*TUS S/E

FN.CARD.ISSUE.ACCOUNT = 'F.CARD.ISSUE.ACCOUNT'
F.CARD.ISSUE.ACCOUNT = ''

* Changed on Feb 21st to Default the Today working day when the COB is started.

LOC.POS = ''

FN.DATES = 'F.DATES'
F.DATES = ''
CALL OPF(FN.DATES,F.DATES)
DATE.ID = ID.COMPANY:'-COB'
CALL F.READ(FN.DATES,DATE.ID,REC.DATE,F.DATE,DATE.ERR)
LST.WORKING.DATE = REC.DATE<EB.DAT.TODAY>
IF NOT(LST.WORKING.DATE) THEN
LST.WORKING.DATE = TODAY
END

* Changes end on Feb 21st

CALL GET.LOC.REF('FUNDS.TRANSFER','TRANS.REF',LOC.POS)

RETURN
*---------------------------------------------------------------------------------
OPEN.FILE:
*---------------------------------------------------------------------------------
CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
*CALL OPF(FN.H.VISA.BATCH.PARAMETER,F.H.VISA.BATCH.PARAMETER)  ;*TUS S/E
CALL OPF(FN.ACCOUNT,F.ACCOUNT)
*CALL OPF(FN.H.LOCAL.PARAMETER,F.H.LOCAL.PARAMETER)  ;*TUS S/E
*CALL OPF(FN.H.CARD.ACCOUNT.CATEGORY,F.H.CARD.ACCOUNT.CATEGORY)  ;*TUS S/E
CALL OPF(FN.CARD.ISSUE.ACCOUNT,F.CARD.ISSUE.ACCOUNT)

RETURN
*---------------------------------------------------------------------------------
PROCESS:
*---------------------------------------------------------------------------------

CALL CACHE.READ(FN.H.VISA.BATCH.PARAMETER,'SYSTEM',R.H.VISA.BATCH.PARAMETER,F.ERROR)
CLAIM.CATEG = R.H.VISA.BATCH.PARAMETER<VB.CLAIM.AC.CATEG>
ARREAR.CATEG = R.H.VISA.BATCH.PARAMETER<VB.ARREAR.AC.CATEG>
CALL CACHE.READ(FN.H.CARD.ACCOUNT.CATEGORY,'SYSTEM',R.H.CARD.ACCOUNT.CATEGORY,ERR.ACC.CATEG)
MAST.CLAIM.CATEG = R.H.CARD.ACCOUNT.CATEGORY<ACC.CATG.MAST.CLAIM.CAT>
MAST.ARREAR.CATEG = R.H.CARD.ACCOUNT.CATEGORY<ACC.CATG.MAST.ARR.CAT>


CALL CACHE.READ(FN.H.LOCAL.PARAMETER,'SYSTEM',R.H.LOCAL.PARAMETER,F.ERROR)
OFS.SOURCE.ID = R.H.LOCAL.PARAMETER<LOC.PARAM.OFS.SRC.ID>


RETURN
END
