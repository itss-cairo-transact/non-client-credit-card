*-----------------------------------------------------------------------------
* <Rating>-23</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.CLEARING.PRELOG.LOAD
*-----------------------------------------------------------------------------
*Company   Name    : Capital Bank Of Jordan
*Developed By      : TAM
*Program   Name    : B.VISA.CLEARING.PRELOG.LOAD
*-------------------------------------------------------------------------------
*Description       : Load routine for the clearing file upload
*Linked With       : BATCH BNK/B.VISA.CLEARING.UPLOAD
*In  Parameter     : NULL
*Out Parameter     : NULL
*ODR  Number       : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
*
*
*---------------------------------------------------------------------------------
$INSERT I_COMMON
$INSERT I_EQUATE
$INSERT I_F.H.VISA.CLEARING.FILE
$INSERT I_F.H.VISA.CLEARING.PARAM
$INSERT I_F.H.UTILITY.FORMAT.PRINT
$INSERT I_B.VISA.CLEARING.COMMON
$INSERT I_F.H.CREDIT.CARD.LOG
$INSERT I_F.CARD.ISSUE
$INSERT I_F.NUMERIC.CURRENCY

    GOSUB INIT
    GOSUB PROCESS

    RETURN
*----------------------------------------------------------------------------------
INIT:
* Initialise all the variables
*----------------------------------------------------------------------------------

    FN.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
    F.NUMERIC.CURRENCY = ''
    CALL OPF(FN.NUMERIC.CURRENCY,F.NUMERIC.CURRENCY)

    FN.H.VISA.CLEARING.FILE='F.H.VISA.CLEARING.FILE'
    F.H.VISA.CLEARING.FILE=''
    CALL OPF(FN.H.VISA.CLEARING.FILE,F.H.VISA.CLEARING.FILE)

    FN.H.VISA.CLEARING.PARAM='F.H.VISA.CLEARING.PARAM'
*   F.H.VISA.CLEARING.PARAM =''    ; *Tus Start
*   CALL OPF(FN.H.VISA.CLEARING.PARAM,F.H.VISA.CLEARING.PARAM)  ; *Tus End

    FN.H.UTILITY.FORMAT.PRINT='F.H.UTILITY.FORMAT.PRINT'
    F.H.UTILITY.FORMAT.PRINT=''
    CALL OPF(FN.H.UTILITY.FORMAT.PRINT,F.H.UTILITY.FORMAT.PRINT)

    FN.H.CREDIT.CARD.LOG ='F.H.CREDIT.CARD.LOG'
    F.H.CREDIT.CARD.LOG =''
    CALL OPF(FN.H.CREDIT.CARD.LOG,F.H.CREDIT.CARD.LOG)

    FN.CARD.ISSUE='F.CARD.ISSUE'
    F.CARD.ISSUE=''
    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)

    FN.ACCOUNT ='F.ACCOUNT'
    F.ACCOUNT =''
    CALL OPF(FN.ACCOUNT,F.ACCOUNT)

    FN.CARD.ISSUE.ACCOUNT = 'F.CARD.ISSUE.ACCOUNT'
    F.CARD.ISSUE.ACCOUNT = ''
    CALL OPF(FN.CARD.ISSUE.ACCOUNT,F.CARD.ISSUE.ACCOUNT)

    RETURN
*----------------------------------------------------------------------------------------
PROCESS:
* Reading the Clearing Parameter and utility format print application and stored the variables
* in common file
*----------------------------------------------------------------------------------------
    CALL CACHE.READ(FN.H.VISA.CLEARING.PARAM,'SYSTEM',R.H.VISA.CLEARING.PARAM,F.ERROR)

    FILE.PATH     = R.H.VISA.CLEARING.PARAM<CL.FILE.PATH>
    ARREAR.CATEG  = R.H.VISA.CLEARING.PARAM<CL.ARREAR.AC.CATEG>
    CLAIM.CATEG   = R.H.VISA.CLEARING.PARAM<CL.CLAIM.AC.CATEG>
    EXCH.RATE.1   = R.H.VISA.CLEARING.PARAM<CL.EXCHANGE.RATE.1>
    EXCH.RATE.2   = R.H.VISA.CLEARING.PARAM<CL.EXCHANGE.RATE.2>
    COMM.RATE.1   = R.H.VISA.CLEARING.PARAM<CL.COMM.PERCENT.1>
    COMM.RATE.2   = R.H.VISA.CLEARING.PARAM<CL.COMM.PERCENT.2>
    ER.POS.RES    = R.H.VISA.CLEARING.PARAM<CL.ERR.POST.REST>
    YR.VALID.IMDS = R.H.VISA.CLEARING.PARAM<CL.VALID.IMDS> 
    CALL F.READ(FN.H.UTILITY.FORMAT.PRINT,'VISA.CLEARING',R.H.UTILITY.FORMAT.PRINT,F.H.UTILITY.FORMAT.PRINT,F.ERR)
    FIELD.NAME   = R.H.UTILITY.FORMAT.PRINT<UF.FIELD.NAME>
    POSITION.ARR = R.H.UTILITY.FORMAT.PRINT<UF.ONLINE.FMT>
    LENGTH.ARR   = R.H.UTILITY.FORMAT.PRINT<UF.DIRECT.DEBIT.FMT>
	
    RETURN
