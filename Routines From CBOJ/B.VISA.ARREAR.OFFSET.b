*-----------------------------------------------------------------------------
* <Rating>-26</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE B.VISA.ARREAR.OFFSET(CARD.NO)
*-----------------------------------------------------------------------------
*Company Name : Capital Bank Of Jordan
*Developed By : Praba
*Program Name : B.VISA.ARREAR.OFFSET
*-------------------------------------------------------------------------------
*Description : Multi threaded routine to offset the arrear account till zero
*Linked With : BNK/B.VISA.ARREAR.OFFSET
*In Parameter : CARD.NO
*Out Parameter : NULL
*ODR Number : ODR-2010-06-0198
*-------------------------------------------------------------------------------
*Modification Details:
*=====================
* Development Credit Card Interface
* 15/06/2011 - Offset added for Master card also.
* 23/01/2012 - PACS00176391
* 28/02/2012 - PACS00183889
* 17/04/2012 - PACS00191480 - Dhinesh
* 12/10/2012 - PACS00201850 - credit card settlement
* 29/05/2013 - PACS00214417 - Account available balance calculated incorrectly,shows INSUFFICIENT FUND error
* 11/12/2013 - PACS00330034 & PACS00334763 issue
* 04/11/2014 - PACS00366021 issue
*---------------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_GTS.COMMON
    $INSERT I_F.OFS.SOURCE
    $INSERT I_F.CARD.ISSUE
    $INSERT I_F.FUNDS.TRANSFER
    $INSERT I_F.ACCOUNT
    $INSERT I_F.DATES
    $INSERT I_F.LIMIT
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_B.VISA.ARREAR.COMMON
    $INSERT I_F.TAM.H.PARAMETER
    $INSERT I_F.H.VISA.ONLINE.PAYMENT
    $INSERT I_F.H.MASTER.ONLINE.PAYMENT
    $INSERT I_F.EB.CONTRACT.BALANCES    ;* TUS S/E

	
	
    GOSUB PROCESS


    RETURN
*---------------------------------------------------------------------------------
PROCESS:
*---------------------------------------------------------------------------------
    FN.LIMIT = "F.LIMIT"
    F.LIMIT = ""
    CALL OPF(FN.LIMIT,F.LIMIT)

    FN.H.VISA.ONLINE.PAYMENT = 'F.H.VISA.ONLINE.PAYMENT'
    F.H.VISA.ONLINE.PAYMENT = ''
    CALL OPF(FN.H.VISA.ONLINE.PAYMENT,F.H.VISA.ONLINE.PAYMENT)

    FN.H.MASTER.ONLINE.PAYMENT = 'F.H.MASTER.ONLINE.PAYMENT'
    F.H.MASTER.ONLINE.PAYMENT = ''
    CALL OPF(FN.H.MASTER.ONLINE.PAYMENT,F.H.MASTER.ONLINE.PAYMENT)


    CHECK.CARD.TYPE = FIELD(CARD.NO,'.',1)
    BEGIN CASE
    CASE CHECK.CARD.TYPE EQ 'VISA'
        GOSUB VISA.PROCESS
    CASE CHECK.CARD.TYPE EQ 'MAST'
        GOSUB MAST.PROCESS
    END CASE

    RETURN

*---------------------------------------------------------------------------------
VISA.PROCESS:
*---------------------------------------------------------------------------------
    CALL F.READ(FN.CARD.ISSUE,CARD.NO,R.CARD.ISSUE,F.CARD.ISSUE,F.ERROR)
    CARD.ACCOUNT = R.CARD.ISSUE<CARD.IS.ACCOUNT>
    CARD.COUNT = DCOUNT(CARD.ACCOUNT,VM)
    CNT=0
    LOOP
        CNT+=1
    WHILE CNT LE CARD.COUNT
        ACCT.NO = R.CARD.ISSUE<CARD.IS.ACCOUNT,CNT>
        CALL F.READ(FN.ACCOUNT,ACCT.NO,R.ACCOUNT,F.ACCOUNT,F.ERR)


*Changes for HVT  ;*Tus Start
*       CALL EB.READ.HVT("EB.CONTRACT.BALANCES",CUST.ACCOUNT,R.ECB.HVT.READ,ECB.HVT.READ.ERR)       ;*Tus End
        THIS.CATEG = R.ACCOUNT<AC.CATEGORY>
        BEGIN CASE
        CASE THIS.CATEG MATCHES ARREAR.CATEG
            ARREAR.ACCOUNT = ACCT.NO
* ARREAR.BALANCE = R.ACCOUNT<AC.WORKING.BALANCE>
* Routine TAM.ACCOUNT.AVAIL.BAL is called to get the Available Balance.
* Since the Forward entries of the contract are affected in Available Bal.
* If Available is NULL then Working bal is taken
            GET.BAL = ACCT.NO

            CALL TAM.ACCOUNT.AVAIL.BAL(GET.BAL)

            ARREAR.BALANCE = GET.BAL

*
            CR.CCY = R.ACCOUNT<AC.CURRENCY>
        CASE NOT(THIS.CATEG MATCHES CLAIM.CATEG) AND NOT(THIS.CATEG MATCHES ARREAR.CATEG)
            CUST.ACCOUNT = ACCT.NO
            CALL EB.READ.HVT("EB.CONTRACT.BALANCES",CUST.ACCOUNT,R.ECB.HVT.READ,ECB.HVT.READ.ERR)
        END CASE
    REPEAT

    GOSUB NEXT.LEVEL

    RETURN

*---------------------------------------------------------------------------------
MAST.PROCESS:
*---------------------------------------------------------------------------------
    CALL F.READ(FN.CARD.ISSUE,CARD.NO,R.CARD.ISSUE,F.CARD.ISSUE,F.ERROR)
    CARD.ACCOUNT = R.CARD.ISSUE<CARD.IS.ACCOUNT>
    CARD.COUNT = DCOUNT(CARD.ACCOUNT,VM)
    CNT=0
    LOOP
        CNT+=1
    WHILE CNT LE CARD.COUNT
        ACCT.NO = R.CARD.ISSUE<CARD.IS.ACCOUNT,CNT>
        CALL F.READ(FN.ACCOUNT,ACCT.NO,R.ACCOUNT,F.ACCOUNT,F.ERR)


*Changes for HVT  ;*Tus Start
        CALL EB.READ.HVT("EB.CONTRACT.BALANCES",ACCT.NO,R.ECB.HVT.READ.1,ECB.HVT.READ.ERR.1)        ;*Tus End
        THIS.CATEG = R.ACCOUNT<AC.CATEGORY>
        BEGIN CASE
        CASE THIS.CATEG MATCHES MAST.ARREAR.CATEG
            ARREAR.ACCOUNT = ACCT.NO
* ARREAR.BALANCE = R.ACCOUNT<AC.WORKING.BALANCE>

* Routine TAM.ACCOUNT.AVAIL.BAL is called to get the Available Balance.
* Since the Forward entries of the contract are affected in Available Bal.
* If Available is NULL then Working bal is taken
            GET.BAL = ACCT.NO
            CALL TAM.ACCOUNT.AVAIL.BAL(GET.BAL)
            ARREAR.BALANCE = GET.BAL
*
            CR.CCY = R.ACCOUNT<AC.CURRENCY>
        CASE NOT(THIS.CATEG MATCHES MAST.CLAIM.CATEG) AND NOT(THIS.CATEG MATCHES MAST.ARREAR.CATEG)
            CUST.ACCOUNT = ACCT.NO
        END CASE
    REPEAT
    GOSUB NEXT.LEVEL

    RETURN
*---------------------------------------------------------------------------------
NEXT.LEVEL:
*---------------------------------------------------------------------------------

* * PACS00366021 - S
* ARREAR.BALANCE = R.ACCOUNT<AC.WORKING.BALANCE>
* PACS00366021 - E
*CR.CCY = R.ACCOUNT<AC.CURRENCY>

    IF ARREAR.BALANCE NE 0 THEN
* PACS00330034 - S
* * PACS00366021 - S
        CALL F.READU(FN.ACCOUNT,CUST.ACCOUNT,R.ACC,F.ACCOUNT,F.ERR,"")


* PACS00366021 - E
* CALL F.READ(FN.ACCOUNT,CUST.ACCOUNT,R.ACC,F.ACCOUNT,F.ERR)
*PACS00338442 - START
* IF R.ACC<AC.AVAILABLE.DATE> AND R.ACC<AC.AVAILABLE.DATE,1> EQ TODAY THEN
        Y.CHK.DATE = TODAY
*HVT Changes OPEN.AVAILABLE.BAL
* Y.AVAIL.ALL.DATE = R.ACC<AC.AVAILABLE.DATE> ;*Tus Start
        Y.AVAIL.ALL.DATE = R.ECB.HVT.READ<ECB.AVAILABLE.DATE>         ;*Tus End
        LOCATE Y.CHK.DATE IN Y.AVAIL.ALL.DATE<1,1> SETTING Y.POS THEN
            CHK.TOT.BAL = R.ACC<AC.AVAILABLE.BAL,Y.POS>
*PACS00338442 - END
        END ELSE
*HVT Changes WORKING.BALANCE
* CHK.TOT.BAL = R.ACC<AC.WORKING.BALANCE> ;*Tus Start
            CHK.TOT.BAL = R.ECB.HVT.READ<ECB.WORKING.BALANCE>         ;*Tus End
        END

* PACS00330034 - E
* CUST.BALANCE = R.ACC<AC.WORKING.BALANCE>

* Routine TAM.ACCOUNT.AVAIL.BAL is called to get the Available Balance.
* Since the Forward entries of the contract are affected in Available Bal.
* If Available is NULL then Working bal is taken
        GET.BAL = CUST.ACCOUNT
        CALL TAM.ACCOUNT.AVAIL.BAL(GET.BAL)
        CUST.BALANCE = GET.BAL
        GOSUB GET.LIMIT.AMT
*PACS00214417 - S
*GOSUB CHECK.ACTUAL.BALANCE
*PACS00214417 - E

        DR.CCY = R.ACC<AC.CURRENCY>
* IF ARREAR.BALANCE LT 0 AND CUST.BALANCE GT 0 THEN

* PACS00422565 - S

        GOSUB CHECK.LOCKED.AMOUNT

        IF ARREAR.BALANCE LT 0 AND CUST.BALANCE GT 0 OR ARREAR.BALANCE GT 0 AND CUST.BALANCE GT 0 THEN

* PACS00422565 - E

            GOSUB ACCOUNT.ENTRIES
        END
    END

    RETURN
*-------------------------------------------------------------------------------------
CHECK.LOCKED.AMOUNT:
*-------------------------------------------------------------------------------------
    U.LOCK.AMT =0
    IF R.ACC<AC.FROM.DATE> THEN
        U.CTR = DCOUNT(R.ACC<AC.FROM.DATE>,VM)
        U.I = 1
        LOOP
        WHILE U.I LE U.CTR
            IF TODAY GE R.ACC<AC.FROM.DATE, U.I> THEN
                U.LOCK.AMT + = R.ACC<AC.LOCKED.AMOUNT,U.I>
            END
            U.I+=1
        REPEAT

        IF CUST.BALANCE LT 0 THEN
            CUST.BALANCE = ABS(CUST.BALANCE) + U.LOCK.AMT
            CUST.BALANCE = CUST.BALANCE * -1

        END ELSE
            CUST.BALANCE = CUST.BALANCE - U.LOCK.AMT
        END

    END
    RETURN
*---------------------------------------------------------------------------------
CHECK.ACTUAL.BALANCE:
*---------------------------------------------------------------------------------

* PACS00191480 - Start
    CHK.BALANCE = 0
    ARR.ACC.ARRAY = ''
    CALL F.READ(FN.CARD.ISSUE.ACCOUNT,CUST.ACCOUNT,R.CARD.ISSUE.ACT,F.CARD.ISSUE.ACCOUNT,CRD.ISS.ACT.ERR)
    IF CRD.ISS.ACT.ERR EQ '' THEN
        LOOP
            REMOVE CRD.NO FROM R.CARD.ISSUE.ACT SETTING CRD.POS
        WHILE CRD.NO:CRD.POS
            IF CRD.NO[1,4] EQ 'MAST' OR CRD.NO[1,4] EQ 'VISA' THEN
                F.CDR.ERROR = ''
                CALL F.READ(FN.CARD.ISSUE,CRD.NO,R.CRD.ISSUE,F.CARD.ISSUE,F.CDR.ERROR)
                GOSUB GET.ARR.BAL
* PACS00201850 - S
                CHK.BALANCE = ABS(ARREAR.BALANCE) + CHK.BALANCE
* PACS00201850 - E
            END
* CHK.BALANCE = ABS(ARREAR.BALANCE) + CHK.BALANCE
            IF CRD.NO EQ CARD.NO THEN
                IF CUST.BALANCE LT CHK.BALANCE THEN
                    CHK.BALANCE = CHK.BALANCE - ABS(ARREAR.BALANCE)
                    CUST.BALANCE = CUST.BALANCE - CHK.BALANCE
                END
                RETURN
            END
        REPEAT
    END

    RETURN

*---------------------------------------------------------------------------------
GET.ARR.BAL:
*---------------------------------------------------------------------------------

    ARREAR.BALANCE = ''
    IF F.CDR.ERROR EQ '' THEN
        CHECK.CARD.ACCOUNT = R.CRD.ISSUE<CARD.IS.ACCOUNT>
        CHANGE VM TO FM IN CHECK.CARD.ACCOUNT
        LOOP
            REMOVE ACCT.NO FROM CHECK.CARD.ACCOUNT SETTING AC.POS
        WHILE ACCT.NO:AC.POS
            LOCATE ACCT.NO IN ARR.ACC.ARRAY SETTING ARR.ACC.POS ELSE
                CALL F.READ(FN.ACCOUNT,ACCT.NO,R.ACCOUNT,F.ACCOUNT,F.ERR)




                THIS.CATEG = R.ACCOUNT<AC.CATEGORY>
                IF THIS.CATEG MATCHES MAST.ARREAR.CATEG AND CRD.NO[1,4] EQ 'MAST' THEN
*HVT Changes WORKING.BALANCE
* ARREAR.BALANCE = R.ACCOUNT<AC.WORKING.BALANCE> ;*Tus Start
                    ARREAR.BALANCE = R.ECB.HVT.READ.1<ECB.WORKING.BALANCE>      ;*Tus End
                    ARR.ACC.ARRAY<-1>= ACCT.NO
                END
                IF THIS.CATEG MATCHES ARREAR.CATEG AND CRD.NO[1,4] EQ 'VISA' THEN
*HVT Changes WORKING.BALANCE
* ARREAR.BALANCE = R.ACCOUNT<AC.WORKING.BALANCE> ;*Tus Start
                    ARREAR.BALANCE = R.ECB.HVT.READ.1<ECB.WORKING.BALANCE>      ;*Tus End
                    ARR.ACC.ARRAY<-1>= ACCT.NO
                END
            END
        REPEAT
    END

    RETURN

* PACS00191480 - End
*---------------------------------------------------------------------------------
GET.LIMIT.AMT:
*---------------------------------------------------------------------------------
* PACS00176391 - Start
    Y.CUSTOMER = R.ACC<AC.CUSTOMER>
    LIMIT.REF = R.ACC<AC.LIMIT.REF,1>
    IF LIMIT.REF NE '' THEN
        O.DATA = Y.CUSTOMER:'.':FMT(LIMIT.REF,'R%10')
* PACS00330034 - S
        CALL F.READ(FN.LIMIT,O.DATA,R.LIMIT,F.LIMIT,LIM.ERR)
        EXP.DATE = R.LIMIT<LI.EXPIRY.DATE>
* PACS00330034 - E
        CALL E.MB.GET.BALANCE

*PACS00214417 - S
*CUST.BALANCE = O.DATA + CUST.BALANCE
        CUST.BALANCE = O.DATA
        CHK.TOT.BAL = CUST.BALANCE
*PACS00214417 - E

    END
* PACS00176391 - End
    RETURN

*---------------------------------------------------------------------------------
ACCOUNT.ENTRIES:
*---------------------------------------------------------------------------------

    CUST.BALANCE = ABS(CUST.BALANCE)
    ARREAR.BALANCE = ABS(ARREAR.BALANCE)
    IF CUST.BALANCE LE ARREAR.BALANCE THEN
        AMOUNT = CUST.BALANCE
    END ELSE
        AMOUNT = ABS(ARREAR.BALANCE)
    END

* Not to Post Transactions for Amount equal to Zero. PACS00330034 - Balance is insufficient then don't post this transaction & check limit expiry.
    IF AMOUNT EQ '0' OR CHK.TOT.BAL LT AMOUNT THEN
*PACS00338442 - START
        Y.ERR.DESC = "Unauthorised overdraft"
        BEGIN CASE
        CASE CHECK.CARD.TYPE EQ 'MAST'
            GOSUB MAST.ERROR.PROCESS
        CASE CHECK.CARD.TYPE EQ 'VISA'
            GOSUB VISA.ERROR.PROCESS
        END CASE
*PACS00338442 - END
        RETURN
    END

    IF EXP.DATE AND EXP.DATE LT TODAY THEN
*PACS00338442 - START
        Y.ERR.DESC = "Limit Expired"
        BEGIN CASE
        CASE CHECK.CARD.TYPE EQ 'MAST'
            GOSUB MAST.ERROR.PROCESS
        CASE CHECK.CARD.TYPE EQ 'VISA'
            GOSUB VISA.ERROR.PROCESS
        END CASE
*PACS00338442 - END
        RETURN
    END


    DR.ACCT = CUST.ACCOUNT
    CR.ACCT = ARREAR.ACCOUNT
    APPLICATION = 'FUNDS.TRANSFER'
    OFS.FUNCTION = 'I'
    PROCESS = 'PROCESS'
    OFS.VERSION = 'FUNDS.TRANSFER,VISA.OFFSET'
    TRANSACTION.ID = ''

* Changed on Feb 21st
    DR.CR.VALUE.DATE = LST.WORKING.DATE
* End

    R.FUNDS.TRANSFER<FT.DEBIT.ACCT.NO> = DR.ACCT
    R.FUNDS.TRANSFER<FT.CREDIT.ACCT.NO> = CR.ACCT
    R.FUNDS.TRANSFER<FT.DEBIT.CURRENCY> = DR.CCY
    R.FUNDS.TRANSFER<FT.CREDIT.AMOUNT> = AMOUNT
    R.FUNDS.TRANSFER<FT.CREDIT.CURRENCY> = CR.CCY
    R.FUNDS.TRANSFER<FT.DEBIT.THEIR.REF> = FIELD(CARD.NO,'.',2)
    R.FUNDS.TRANSFER<FT.CREDIT.THEIR.REF> = FIELD(CARD.NO,'.',2)
* To do the transaction in last working day.
    R.FUNDS.TRANSFER<FT.CREDIT.VALUE.DATE> = DR.CR.VALUE.DATE
    R.FUNDS.TRANSFER<FT.DEBIT.VALUE.DATE> = DR.CR.VALUE.DATE
    R.FUNDS.TRANSFER<FT.LOCAL.REF,LOC.POS> = CARD.NO

    GTS.MODE = ''
    NO.OF.AUTH = 0

    CALL OFS.BUILD.RECORD(APPLICATION,OFS.FUNCTION,PROCESS,OFS.VERSION,GTS.MODE,NO.OF.AUTH,TRANSACTION.ID,R.FUNDS.TRANSFER,OFS.RECORD)
    OFS.MESSAGE = OFS.RECORD
    OFS.MSG.ID = ''
    OPTIONS = ''
* PACS00366021 - S
    FN.OFS.SRC = "F.OFS.SOURCE"
    F.OFS.SRC = ""
    CALL OPF(FN.OFS.SRC,F.OFS.SRC)

    OFS.SOURC.ID = 'OFS.LOAD'
    OFS$SOURCE.ID = OFS.SOURC.ID
    CALL F.READ(FN.OFS.SRC,OFS.SOURC.ID,R.OFS.SRC,F.OFS.SRC,OFS.ERR)
    OFS$SOURCE.REC = R.OFS.SRC
    OFS.RESP = ''
    OPTIONS = ''

* CALL OFS.POST.MESSAGE(OFS.MESSAGE,OFS.MSG.ID,OFS.SOURCE.ID,OPTIONS)

    CALL OFS.BULK.MANAGER(OFS.MESSAGE,OFS.RESP,OPTIONS)
* PACS00366021 - E

    RETURN
*PACS00338442 - START
*******************
MAST.ERROR.PROCESS:
*******************
    R.MASTER.ONLINE.PAYMENT<MOL.CARD.NUMBER> = FIELD(CARD.NO,'.',2)
    R.MASTER.ONLINE.PAYMENT<MOL.ARREAR.ACCOUNT> = ARREAR.ACCOUNT
    R.MASTER.ONLINE.PAYMENT<MOL.TRANSACTION.REF> = "ERROR"
    R.MASTER.ONLINE.PAYMENT<MOL.DATE> = R.DATES(EB.DAT.LAST.WORKING.DAY)
    R.MASTER.ONLINE.PAYMENT<MOL.PAYMENT.TYPE> = 'ACCOUNT'
    R.MASTER.ONLINE.PAYMENT<MOL.NARRATIVE> = Y.ERR.DESC

    APPLICATION = 'H.MASTER.ONLINE.PAYMENT'
    OFS.FUNCTION = 'I'
    PROCESS = 'PROCESS'
    OFS.VERSION = 'H.MASTER.ONLINE.PAYMENT,OFFSET'
    TRANSACTION.ID = ''

    GTS.MODE = ''
    NO.OF.AUTH = 0

    CALL OFS.BUILD.RECORD(APPLICATION,OFS.FUNCTION,PROCESS,OFS.VERSION,GTS.MODE,NO.OF.AUTH,TRANSACTION.ID,R.MASTER.ONLINE.PAYMENT,OFS.RECORD)
    OFS.MESSAGE = OFS.RECORD
    OFS.MSG.ID = ''
    OPTIONS = ''
    CALL OFS.POST.MESSAGE(OFS.MESSAGE,OFS.MSG.ID,OFS.SOURCE.ID,OPTIONS)

    RETURN
*******************
VISA.ERROR.PROCESS:
*******************
    R.H.VISA.ONLINE.PAYMENT<VOL.CARD.NUMBER> = FIELD(CARD.NO,'.',2)
    R.H.VISA.ONLINE.PAYMENT<VOL.MXP.ACCT.NO> = CUST.ACCOUNT
    R.H.VISA.ONLINE.PAYMENT<VOL.TRANS.DATE> = R.DATES(EB.DAT.LAST.WORKING.DAY)
    R.H.VISA.ONLINE.PAYMENT<VOL.TRANS.REF> = "ERROR"
    R.H.VISA.ONLINE.PAYMENT<VOL.PAYMENT.TYPE> = 'ACCOUNT'
    R.H.VISA.ONLINE.PAYMENT<VOL.NARRATIVE> = Y.ERR.DESC


    APPLICATION = 'H.VISA.ONLINE.PAYMENT'
    OFS.FUNCTION = 'I'
    PROCESS = 'PROCESS'
    OFS.VERSION = 'H.VISA.ONLINE.PAYMENT,OFFSET'
    TRANSACTION.ID = ''

    GTS.MODE = ''
    NO.OF.AUTH = 0

    CALL OFS.BUILD.RECORD(APPLICATION,OFS.FUNCTION,PROCESS,OFS.VERSION,GTS.MODE,NO.OF.AUTH,TRANSACTION.ID,R.H.VISA.ONLINE.PAYMENT,OFS.RECORD)
    OFS.MESSAGE = OFS.RECORD
    OFS.MSG.ID = ''
    OPTIONS = ''
    CALL OFS.POST.MESSAGE(OFS.MESSAGE,OFS.MSG.ID,OFS.SOURCE.ID,OPTIONS)

    RETURN
*PACS00338442 - END
END
